# devecommerce

Clone the repository

    git clone git@gitlab.com:rodineiti/devecommerce.git

Switch to the repo folder

    cd devecommerce
    cp config-example.php config.php
    
Edit file config.php, and set connection mysql

    $config["dbname"] = "devecommerce";
    $config["dbhost"] = "mysql";
    $config["dbuser"] = "root";
    $config["dbpass"] = "root";

Dump file devecommerce.sql into database

Run server php or your server (Wamp, Mamp, Xamp), and open in the browser localhost:2020
  
    php -S localhost:2020

Url ADMIN:

    Admin login: http://localhost:2020/devecommerce/admin?login

    login: admin@admin.com
    password: 123456

Prints:

Dashboard Admin

![image](https://user-images.githubusercontent.com/25492122/89692501-4597b480-d8e2-11ea-8fad-d33e379a7b4a.png)