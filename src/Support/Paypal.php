<?php

namespace Src\Support;

use Src\Models\Cart;
use Src\Models\Purchase;

class Paypal
{
    private $clientId;
    private $secretKey;

    public function __construct($clientId = CONF_CLIENTID_PAYPAL, $secretKey = CONF_SECRETKEY_PAYPAL)
    {
        $this->clientId = $clientId;
        $this->secretKey = $secretKey;
    }

    public function create($reference, $data)
    {
        try {

            $apiContext = new \PayPal\Rest\ApiContext(
                new \PayPal\Auth\OAuthTokenCredential(
                    $this->clientId, $this->secretKey
                )
            );

            $payer = new \PayPal\Api\Payer();
            $payer->setPaymentMethod("paypal");

            $amout = new \PayPal\Api\Amount();
            $amout->setCurrency("BRL")->setTotal((new Cart())->total());

            $transaction = new \PayPal\Api\Transaction();
            $transaction->setAmount($amout);
            $transaction->setInvoiceNumber($reference);

            $redirectUrls = new \PayPal\Api\RedirectUrls();
            $redirectUrls->setReturnUrl(BASE_URL . "paypal/success");
            $redirectUrls->setCancelUrl(BASE_URL . "paypal/cancel");

            $payment = new \PayPal\Api\Payment();
            $payment->setIntent("sale");
            $payment->setPayer($payer);
            $payment->setTransactions([
                $transaction
            ]);
            $payment->setRedirectUrls($redirectUrls);

            $payment->create($apiContext);

            $link_redirect = $payment->getApprovalLink();
            return json_encode(["error" => false, "link_redirect" => $link_redirect]);
        } catch (\PayPal\Exception\PayPalConnectionException $exception) {
            return json_encode(["error" => true, "message" => $exception->getMessage()]);
        }
    }

    public function notification($reference, $payerId)
    {
        try {

            $apiContext = new \PayPal\Rest\ApiContext(
                new \PayPal\Auth\OAuthTokenCredential(
                    $this->clientId, $this->secretKey
                )
            );

            $payment = \PayPal\Api\Payment::get($reference, $apiContext);

            $execute = new \PayPal\Api\PaymentExecution();
            $execute->setPayerId($payerId);

            $result = $payment->execute($execute, $apiContext);

            try {
                $payment = \PayPal\Api\Payment::get($reference, $apiContext);
                $status = $payment->getState();
                $t = current($payment->getTransactions());
                $t = $t->toArray();
                $id = $t["invoice_number"];

                if ($status === "approved") {
                    $status = 3;
                    (new Cart())->destroy();
                } else {
                    $status = 7; // canceled
                }

                $purchase = (new Purchase())->getById($id);

                if ($purchase) {
                    (new Purchase())->updateData($purchase->id, ["payment_status" => $status]);
                }
                return json_encode(["error" => false, "message" => "Notificação com sucesso"]);
            } catch (\Exception $exception) {
                return json_encode(["error" => true, "message" => $exception->getMessage()]);
            }
        } catch (\PayPal\Exception\PayPalConnectionException $exception) {
            return json_encode(["error" => true, "message" => $exception->getMessage()]);
        }
    }
}