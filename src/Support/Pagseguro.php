<?php

namespace Src\Support;

use Src\Models\Cart;
use Src\Models\Purchase;

class Pagseguro
{
    public static function getSession()
    {
        try {
            $response = \PagSeguro\Services\Session::create(
                \PagSeguro\Configuration\Configure::getAccountCredentials()
            );

            return $response->getResult();
        } catch (\Exception $exception) {
            echo $exception->getMessage();
            return null;
        }
    }

    public static function creditCard($reference, array $data)
    {
        $card = new \PagSeguro\Domains\Requests\DirectPayment\CreditCard();
        $card->setReceiverEmail(CONF_EMAIL_PAGSEGURO);
        $card->setReference($reference);
        $card->setCurrency("BRL");

        $items = (new Cart())->get();
        foreach ($items as $value) {
            $card->addItems()->withParameters(
                $value["id"],
                $value["name"],
                intval($value["qty"]),
                floatval($value["price"])
            );
        }

        $card->setSender()->setName($data["name"]);
        $card->setSender()->setEmail($data["email"]);
        $card->setSender()->setDocument()->withParameters("CPF", $data["cpf"]);
        $card->setSender()->setPhone()->withParameters($data["dddPhone"], $data["phone"]);

        $card->setSender()->setHash($data["id"]);
        $card->setSender()->setIp($_SERVER["REMOTE_ADDR"] < 9 ? "127.0.0.1" : $_SERVER["REMOTE_ADDR"]);

        $card->setShipping()->setAddress()->withParameters(
            $data["address"],
            $data["number"],
            $data["district"],
            $data["cep"],
            $data["city"],
            $data["state"],
            "BRA",
            $data["complement"]
        );

        $card->setBilling()->setAddress()->withParameters(
            $data["address"],
            $data["number"],
            $data["district"],
            $data["cep"],
            $data["city"],
            $data["state"],
            "BRA",
            $data["complement"]
        );

        $card->setToken($data["cardToken"]);
        $cardInstallment = explode(";", $data["cardInstallment"]);
        $card->setInstallment()->withParameters($cardInstallment[0], $cardInstallment[1]);
        $card->setHolder()->setName($data["cardOwner"]);
        $card->setHolder()->setDocument()->withParameters("CPF", $data["cardCpfOwner"]);
        $card->setMode("DEFAULT");

        $card->setNotificationUrl(BASE_URL . "pagseguro/notification");

        try {
            $result = $card->register(\PagSeguro\Configuration\Configure::getAccountCredentials());
            return json_encode(["error" => false, "message" => $result]);
        } catch (\Exception $exception) {
            return json_encode(["error" => true, "message" => $exception->getMessage()]);
        }
    }

    public static function hasPost()
    {
        try {
            $response = null;

            if (\PagSeguro\Helpers\Xhr::hasPost()) {
                $response = \PagSeguro\Services\Transactions\Notification::check(
                    \PagSeguro\Configuration\Configure::getAccountCredentials()
                );
                /**
                 * 1 = Aguardando pagamento
                 * 2 = Em análise
                 * 3 = Paga
                 * 4 = Disponível
                 * 5 = Em disputa
                 * 6 = Devolvido
                 * 7 = Cancelado
                 * 8 = Debitado
                 * 9 = Retenção temporária = chargeback
                 */
                $reference = $response->getReference();
                $status = $response->getStatus();

                $purchase = (new Purchase())->getById($reference);

                if ($purchase) {
                    (new Purchase())->updateData($purchase->id, ["payment_status" => $status]);
                }
            }

            return json_encode(["error" => false, "message" => "Atualizado status na notificação com sucesso"]);
        } catch (\Exception $exception) {
            return json_encode(["error" => true, "message" => $exception->getMessage()]);
        }
    }
}