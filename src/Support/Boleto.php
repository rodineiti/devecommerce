<?php

namespace Src\Support;

use Src\Models\Cart;
use Src\Models\Purchase;
use Gerencianet\Exception\GerencianetException;
use Gerencianet\Gerencianet;

class Boleto
{
    private $clientId;
    private $secretKey;

    public function __construct($clientId = CONF_CLIENTID_GERENCIANET, $secretKey = CONF_SECRETKEY_GERENCIANET)
    {
        $this->clientId = $clientId;
        $this->secretKey = $secretKey;
    }

    public function create($reference, $data)
    {
        try {

            $options = [
                "client_id" => $this->clientId,
                "client_secret" => $this->secretKey,
                "sandbox" => CONF_SECRETKEY_GERENCIANET_SANDBOX
            ];

            $products = (new Cart())->get();
            $items = array();
            foreach ($products as $value) {
                $items[] = [
                    "name" => $value["name"],
                    "amount" => intval($value["qty"]),
                    "value" => ($value["price"] * 100) // converte pra centavos
                ];
            }

            $body = [
                "metadata" => [
                    "custom_id" => $reference,
                    "notification_url" => BASE_URL . "boleto/notification"
                ],
                "items" => $items,
                "shippings" => [
                    [
                        "name" => "FRETE",
                        "value" => ((new Cart())->frete() * 100) // converte pra centavos
                    ]
                ]
            ];

            $api = new Gerencianet($options);
            $charge = $api->createCharge([], $body);

            if ($charge["code"] === 200) {
                $charge_id = $charge["data"]["charge_id"];

                $params = ["id" => $charge_id];

                $customer = [
                    "name" => $data["name"],
                    "cpf" => $data["cpf"],
                    "phone_number" => $data["dddPhone"] . "" . $data["phone"],
                ];

                $bankingBillet = [
                    "expire_at" => date("Y-m-d", strtotime("+4 days")),
                    "customer" => $customer,
                    "message" => "COMPRA DE TESTE " . SITE_NAME
                ];

                $payment = [
                    "banking_billet" => $bankingBillet
                ];

                $body = [
                    "payment" => $payment
                ];

                try {
                    $charge = $api->payCharge($params, $body);

                    if ($charge["code"] === 200) {
                        $link_redirect = $charge["data"]["link"];
                        (new Cart())->destroy();
                        return json_encode(["error" => false, "link_redirect" => $link_redirect]);
                    } else {
                        return json_encode(["error" => true, "message" => "Erro ao gerar pagamento"]);
                    }
                } catch (GerencianetException  $exception) {
                    return json_encode(["error" => true, "message" => $exception->getMessage(), "try" => 0]);
                }
            } else {
                return json_encode(["error" => true, "message" => "Erro ao processar pagamento"]);
            }
        } catch (GerencianetException  $exception) {
            return json_encode(["error" => true, "message" => $exception->getMessage(), "try" => 1]);
        } catch (\Exception $exception) {
            return json_encode(["error" => true, "message" => $exception->getMessage(), "try" => 2]);
        }
    }

    public function notification($notificationToken)
    {
        try {

            $options = [
                "client_id" => $this->clientId,
                "client_secret" => $this->secretKey,
                "sandbox" => CONF_SECRETKEY_GERENCIANET_SANDBOX
            ];

            $params = ["token" => $notificationToken];

            $api = new Gerencianet($options);
            $charge = $api->getNotification($params, []);

            $data = end($charge["data"]);
            $custom_id = $data["custom_id"];
            $status = $data["status"]["current"];

            if ($status === "paid") {
                $status = 3;
            } else {
                $status = 7; // canceled
            }

            $purchase = (new Purchase())->getById($custom_id);

            if ($purchase) {
                (new Purchase())->updateData($purchase->id, ["payment_status" => $status]);
            }
            return json_encode(["error" => false, "message" => "Notificação com sucesso"]);

        } catch (GerencianetException  $exception) {
            return json_encode(["error" => true, "message" => $exception->getMessage(), "try" => 1]);
        } catch (\Exception $exception) {
            return json_encode(["error" => true, "message" => $exception->getMessage(), "try" => 2]);
        }
    }
}