<?php

namespace Src\Support;

use Src\Models\Cart;
use Src\Models\Purchase;

class Mercadopago
{
    private $apiKey;
    private $secretKey;

    public function __construct($apiKey = CONF_APIKEY_MERCADO_PAGO, $secretKey = CONF_SECRETKEY_MERCADO_PAGO)
    {
        $this->apiKey = $apiKey;
        $this->secretKey = $secretKey;
    }

    public function create($reference, $data)
    {
        try {
            $data = [
                "items" => [],
                "shipments" => [
                    "mode" => "custom",
                    "cost" => (new Cart())->frete(),
                    "receiver_address" => [
                        "zip_code" => $data["cep"]
                    ]
                ],
                "back_urls" => [
                    "success" => BASE_URL . "mercadopago/success",
                    "pending" => BASE_URL . "mercadopago/pending",
                    "failure" => BASE_URL . "mercadopago/failure",
                ],
                "notification_url" => BASE_URL . "mercadopago/notification",
                "auto_return" => "all",
                "external_reference" => $reference,
            ];

            $mp = new \MP($this->apiKey, $this->secretKey);
            $mp->sandbox_mode(CONFIG_MERCADO_PAGO_SANDBOX);

            $items = (new Cart())->get();
            foreach ($items as $value) {
                $data["items"][] = [
                    "title" => $value["name"],
                    "quantity" => $value["qty"],
                    "currency_id" => "BRL",
                    "unit_price" => floatval($value["price"]),
                ];
            }

            $response = $mp->create_preference($data);

            if ($response["status"] === 201) {
                $link_redirect = $response["response"][CONFIG_MERCADO_PAGO_ENVIRONMENT];
                return json_encode(["error" => false, "link_redirect" => $link_redirect]);
            }

            return json_encode(["error" => true, "message" => 2]);

        } catch (\Exception $exception) {
            return json_encode(["error" => true, "message" => $exception->getMessage()]);
        }
    }

    public function notification($id)
    {
        try {
            $mp = new \MP($this->apiKey, $this->secretKey);
            $mp->sandbox_mode(CONFIG_MERCADO_PAGO_SANDBOX);
            $response = $mp->get_payment_info($id);

            if ($response["status"] === 200) {
                $data = $response["response"];
                file_put_contents("mercadopagolog.txt", print_r($data, true));

                /**
                 * penging = Em Análise
                 * approved = Aprovado
                 * in_progress = Em revisão
                 * in_mediantian = Em processo de disputa
                 * rejected = Rejeitado
                 * cancelled = Cancelado
                 * refuded = Reembolso
                 * charged_back = Chargeback
                 */
                $reference = $data["collection"]["external_reference"];
                $status = $data["collection"]["status"];

                if ($status === "penging") {
                    $status = 2;
                } else if ($status === "approved") {
                    $status = 3;
                } else if ($status === "cancelled" || $status === "rejected") {
                    $status = 3;
                } else {
                    $status = 1;
                }

                $purchase = (new Purchase())->getById($reference);

                if ($purchase) {
                    (new Purchase())->updateData($purchase->id, ["payment_status" => $status]);
                }
            }

            return json_encode(["error" => false, "message" => "Notificação com sucesso"]);

        } catch (\Exception $exception) {
            return json_encode(["error" => true, "message" => $exception->getMessage()]);
        }
    }
}