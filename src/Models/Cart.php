<?php

namespace Src\Models;

class Cart
{
    public function add($product, $qty)
    {
        if (!isset($_SESSION["cart"])) {
            $_SESSION["cart"] = array();
        }

        if (isset($_SESSION["cart"][$product->id])) {
            $_SESSION["cart"][$product->id] += $qty;
        } else {
            $_SESSION["cart"][$product->id] = $qty;
        }

        unset($_SESSION["shipping"]);
    }

    public function get()
    {
        $data = array();
        if (isset($_SESSION["cart"])) {
            foreach ($_SESSION["cart"] as $id => $qty) {
                $product = (new Product())->getById($id);
                $data[] = [
                    "id" => $id,
                    "qty" => $qty,
                    "price" => $product->price,
                    "name" => $product->name,
                    "image" => current($product->images)->url,
                    "weight" => $product->weight,
                    "width" => $product->width,
                    "height" => $product->height,
                    "length" => $product->length,
                    "diameter" => $product->diameter,
                ];
            }
        }
        return $data;
    }

    public function del($product)
    {
        if (isset($_SESSION["cart"][$product->id])) {
            unset($_SESSION["cart"][$product->id]);
        }
        unset($_SESSION["shipping"]);
        return true;
    }

    public function inc($product)
    {
        if (isset($_SESSION["cart"][$product->id])) {
            $_SESSION["cart"][$product->id] += 1;
        }
        unset($_SESSION["shipping"]);
        return true;
    }

    public function dec($product)
    {
        if (isset($_SESSION["cart"][$product->id])) {
            if ($_SESSION["cart"][$product->id] > 1) {
                $_SESSION["cart"][$product->id] -= 1;
            } else {
                unset($_SESSION["cart"][$product->id]);
            }
        }
        unset($_SESSION["shipping"]);
        return true;
    }

    public function count()
    {
        $qty = 0;

        $cart = $this->get();
        foreach ($cart as $item) {
            $qty += intval($item["qty"]);
        }

        return $qty;
    }

    public function subtotal()
    {
        $subtotal = 0;

        $cart = $this->get();
        foreach ($cart as $item) {
            $subtotal += (floatval($item["price"]) * intval($item["qty"]));
        }

        return $subtotal;
    }

    public function total()
    {
        $frete = 0;
        $total = $this->subtotal();

        if (isset($_SESSION["shipping"]) && !empty($_SESSION["shipping"])) {
            $shipping = $_SESSION["shipping"];
            $frete = (isset($shipping["price"]) ? floatval($shipping["price"]) : 0);
        }

        return ($total + $frete);
    }

    public function frete()
    {
        $frete = 0;

        if (isset($_SESSION["shipping"]) && !empty($_SESSION["shipping"])) {
            $shipping = $_SESSION["shipping"];
            $frete = (isset($shipping["price"]) ? floatval($shipping["price"]) : 0);
        }

        return $frete;
    }

    public function calculateShipping($cepDestination)
    {
        $array = [
            "price" => 0,
            "date" => "",
            "cep" => $cepDestination
        ];

        $nVlPeso = 0;
        $nVlComprimento = 0;
        $nVlAltura = 0;
        $nVlLargura = 0;
        $nVlDiametro = 0;
        $nVlValorDeclarado = 0;

        $this->setDataCalcCep($nVlPeso, $nVlComprimento, $nVlAltura, $nVlLargura, $nVlDiametro, $nVlValorDeclarado);

        $sum = ($nVlComprimento + $nVlAltura + $nVlLargura);

        if ($sum > CONF_LIMIT_CEP_SHIPPING) { // 200 / 3 variáveis
            $nVlComprimento = 66;
            $nVlAltura = 66;
            $nVlLargura = 66;
        }

        if ($nVlDiametro > CONF_LIMIT_CEP_DIAMETER) {
            $nVlDiametro = 90;
        }

        if ($nVlPeso > CONF_LIMIT_CEP_LENGTH) {
            $nVlPeso = 40;
        }

        $data = [
            "nCdServico" => "40010",
            "sCepOrigem" => CONF_CEP_ORIGEM,
            "sCepDestino" => $cepDestination,
            "nVlPeso" => $nVlPeso,
            "nCdFormato" => "1",
            "nVlComprimento" => $nVlComprimento,
            "nVlAltura" => $nVlAltura,
            "nVlLargura" => $nVlLargura,
            "nVlDiametro" => $nVlDiametro,
            "sCdMaoPropria" => "N",
            "nVlValorDeclarado" => $nVlValorDeclarado,
            "sCdAvisoRecebimento" => "N",
            "StrRetorno" => "xml",
        ];

        $url = "http://ws.correios.com.br/calculador/CalcPrecoprazo.aspx";
        $data = http_build_query($data);

        $curl = curl_init($url."?".$data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        $response = simplexml_load_string($response);

        $array["price"] = str_replace(",",".", current($response->cServico->Valor));
        $array["date"] = current($response->cServico->PrazoEntrega);

        $_SESSION["shipping"] = $array;

        return $array;
    }

    private function setDataCalcCep(&$nVlPeso, &$nVlComprimento, &$nVlAltura, &$nVlLargura, &$nVlDiametro, &$nVlValorDeclarado)
    {
        $cart = $this->get();
        foreach ($cart as $item) {
            $nVlPeso += floatval($item["weight"]);
            $nVlComprimento += floatval($item["length"]);
            $nVlAltura += floatval($item["height"]);
            $nVlLargura += floatval($item["width"]);
            $nVlDiametro += floatval($item["diameter"]);
            $nVlValorDeclarado += (floatval($item["price"]) * floatval($item["qty"]));
        }
    }

    public function destroy()
    {
        unset($_SESSION["cart"]);
        unset($_SESSION["shipping"]);
        return true;
    }
}