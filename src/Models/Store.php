<?php

namespace Src\Models;

class Store
{
    public function getTemplateData()
    {
        $data = array();
        $data["categories"] = (new Category())->all();
        $data["widget_feature_1"] = (new Product())->all(0, 5, ["featured" => '1'], true);
        $data["widget_feature_2"] = (new Product())->all(0, 3, ["featured" => '1'], true);
        $data["widget_sale"] = (new Product())->all(0, 3, ["sale" => '1'], true);
        $data["widget_toprated"] = (new Product())->all(0, 3, ["toprated" => '1']);
        $data["cart_qty"] = (new Cart())->count();
        $data["cart_subtotal"] = (new Cart())->subtotal();
        return $data;
    }
}