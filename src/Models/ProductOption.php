<?php

namespace Src\Models;

use Src\Core\Model;

class ProductOption extends Model
{
    public function __construct()
    {
        parent::__construct("product_options");
    }

    public function getById($id, $columns = ["*"])
    {
        $model = $this->findById($id, $columns);

        if ($model) {
            return $model;
        }
        return null;
    }

    public function getByOption($option_id, $columns = ["*"])
    {
        $results = $this->read(true, ["*"], ["option_id" => $option_id]) ?? [];
        return $results;
    }

    public function getCountOfOption($groups = array(), $ids = array())
    {
        $query = "SELECT description, option_id, count(option_id) as total ";
        $query .= "FROM devecommerce.product_options ";
        $query .= " WHERE option_id IN ('".implode("','", $groups)."') ";
        $query .= " AND product_id IN ('".implode("','", $ids)."') ";
        $query .= "GROUP BY description ORDER BY option_id;";

        $results = $this->customQuery($query) ?? [];
        return $results;
    }

    public function createSeveral($product_id, array $data)
    {
        $this->destroyByProduct($product_id);

        foreach ($data as $option_id => $description) {
            if (!empty($description)) {
                $insert = $this->insert([
                    "product_id" => $product_id,
                    "option_id" => $option_id,
                    "description" => $description
                ]);

                if (!$insert) {
                    return false;
                }
            }
        }
        return true;
    }

    public function getByProduct($product_id)
    {
        $data = array();
        $results = $this->read(true, ["option_id","description"], ["product_id" => $product_id]) ?? [];
        foreach ($results as $result) {
            $data["options"][$result->option_id] = $result->description;
        }
        return $data;
    }

    public function destroyByProduct($product_id)
    {
        $options = $results = $this->read(true, ["id"], ["product_id" => $product_id]) ?? [];
        foreach ($options as $option) {
            $this->delete(["id" => $option->id]);
        }
        return true;
    }
}

?>