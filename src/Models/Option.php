<?php

namespace Src\Models;

use Src\Core\Model;

class Option extends Model
{
    public function __construct()
    {
        parent::__construct("options");
    }

    public function all($where = [])
    {
        $results = $this->read(true, ["*"], $where) ?? [];
        return $results;
    }

    public function getById($id, $columns = ["*"])
    {
        $model = $this->findById($id, $columns);

        if ($model) {
            return $model;
        }
        return null;
    }

    public function create(array $data)
    {
        $modelId = $this->insert($data);

        if ($modelId) {
            return $this->getById($modelId);
        }
        return null;
    }

    public function updateData($id, array $data)
    {
        if ($this->update($data, ["id" => $id])) {
            return $this->getById($id);
        }
        return false;
    }

    public function destroy($id)
    {
        return $this->delete(["id" => $id]);
    }
}

?>