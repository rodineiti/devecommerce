<?php

namespace Src\Models;

use Src\Core\Model;

class Purchase extends Model
{
    public function __construct()
    {
        parent::__construct("purchases");
    }

    public function getById($id, $columns = ["*"])
    {
        $model = $this->findById($id, $columns);

        if ($model) {
            return $model;
        }
        return null;
    }

    public function create(array $data)
    {
        if (!filter_var($data["user_id"], FILTER_VALIDATE_INT)) {
            return false;
        }

        if (empty($data["amount"])) {
            return false;
        }

        if (empty($data["payment_type"])) {
            return false;
        }

        $data["payment_status"] = 1;
        $model = $this->insert($data);
        return $model;
    }

    public function updateData($id, array $data)
    {
        if ($this->update($data, ["id" => $id])) {
            $model = $this->read(false, ["*"], ["id" => $id]);
            return $model;
        }

        return false;
    }
}