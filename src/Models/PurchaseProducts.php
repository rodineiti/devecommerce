<?php

namespace Src\Models;

use Src\Core\Model;

class PurchaseProducts extends Model
{
    public function __construct()
    {
        parent::__construct("purchases_products");
    }

    public function create(array $data)
    {
        if (!filter_var($data["purchase_id"], FILTER_VALIDATE_INT)) {
            return false;
        }

        if (!filter_var($data["product_id"], FILTER_VALIDATE_INT)) {
            return false;
        }

        if (!filter_var($data["qty"], FILTER_VALIDATE_INT)) {
            return false;
        }

        if (empty($data["product_price"])) {
            return false;
        }

        $model = $this->insert($data);
        return $model;
    }
}