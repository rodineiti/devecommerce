<?php

namespace Src\Models;

use Src\Core\Model;
use Src\Support\Session;

class User extends Model
{
    public function __construct()
    {
        parent::__construct("users");
    }

    public function all($where = [])
    {
        $users = $this->read(true, ["*"], $where) ?? [];
        return $users;
    }

    public function attempt($email, $password)
    {
        $user = $this->read(false, ["*"], ["email" => $email]);

        if (!$user) {
            return false;
        }

        if (!pwd_verify($password, $user->password)) {
            return false;
        }

        if (pwd_rehash($user->password)) {
            $this->update(["password" => pwd_gen_hash($password)],  ["id" => $user->id]);
        }

        return $user;
    }

    public function setSession($user)
    {
        Session::set("userLogged", (object)$user);
    }

    public function destroySession()
    {
        Session::destroy("userLogged");
    }

    public function create(array $data)
    {
        if (!filter_var($data["email"], FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        if ($this->exists("email", $data["email"])) {
            return false;
        }

        $data["password"] = pwd_gen_hash($data["password"]);
        $userId = $this->insert($data);

        if ($userId) {
            return $this->getById($userId);
        }

        return null;
    }

    public function getById($id, $columns = ["*"])
    {
        $user = $this->findById($id, $columns);

        if ($user) {
            return $user;
        }
        return null;
    }

    public function updateProfile($id, array $data)
    {
        $newData = array();
        if (!empty($data["email"])) {
            $newData["email"] = $data["email"];
        }

        if (!empty($data["name"])) {
            $newData["name"] = $data["name"];
        }

        if (!empty($data["password"])) {
            $newData["password"] = pwd_gen_hash($data["password"]);
        }

        if (isset($newData["email"]) && !filter_var($newData["email"], FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        if (isset($newData["email"]) && $this->exists("email", $newData["email"], $id)) {
            return false;
        }

        unset($newData["email"]);

        if (count($newData)) {
            if ($this->update($newData, ["id" => $id])) {
                $user = $this->read(false, ["*"], ["id" => $id]);
                return $user;
            }
        }

        return false;
    }

    public function exists($field, $value, $id = null)
    {
        if ($id) {
            return $this->read(false, ["*"], [$field => $value], ["id", "NOT IN", [$id]]);
        }

        return $this->read(false, ["*"], [$field => $value]);
    }

    public function destroy($id)
    {
        $purchases = (new Purchase())->read(true, ["*"], ["user_id" => $id]);
        foreach ($purchases as $purchase) {
            (new Purchase())->delete(["id" => $purchase->id]);
        }
        $rates = (new ProductRate())->read(true, ["*"], ["user_id" => $id]);
        foreach ($rates as $rate) {
            (new ProductRate())->delete(["id" => $rate->id]);
        }
        return $this->delete(["id" => $id]);
    }
}

?>