<?php

namespace Src\Models;

use Src\Core\Model;

class Product extends Model
{
    public function __construct()
    {
        parent::__construct("products");
    }

    public function all($offset = null, $limit = null, $filters = [], $random = false)
    {
        $query = "SELECT * FROM products ";

        $order = null;

        if ($random) {
            $order = " ORDER BY RAND() ";
        }

        if (!empty($filters["toprated"])) {
            $order = " ORDER BY products.rating DESC ";
            unset($filters["toprated"]);
        }

        if (count($filters)) {
            $where = $this->buildWhere($filters);
            $query .= " WHERE " . implode(" AND ", $where);
        }

        if ($order) {
            $query .= $order;
        }

        if ($limit) {
            $query .= " LIMIT {$limit} ";
        }

        if ($offset) {
            $query .= " OFFSET {$offset} ";
        }

        $results = $this->customQuery($query) ?? [];

        foreach ($results as $result) {
            $brand = (new Brand())->getById($result->brand_id, ["name"]);
            $category = (new Category())->getById($result->category_id, ["name"]);
            $result->brand = $brand->name;
            $result->category = $category->name;
            $result->images = (new ProductImage())->getByProduct($result->id);
        }

        return $results;
    }

    public function getMaxPrice()
    {
        $query = "SELECT products.price ";
        $query .= "FROM products ";
        $query .= "ORDER BY products.price DESC LIMIT 1;";

        $result = $this->customQuery($query, false) ?? [];

        if ($result) {
            return $result->price;
        }
        return 0;
    }

    public function getOfBrand($filters = [])
    {
        $query = "SELECT brands.id, brands.name, count(products.id) as total ";
        $query .= "FROM products ";
        $query .= "INNER JOIN brands ON (brands.id = products.brand_id) ";

        if (count($filters)) {
            $where = $this->buildWhere($filters);
            $query .= " WHERE " . implode(" AND ", $where);
        }

        $query .= " GROUP BY brands.id, brands.name, products.brand_id ";
        $query .= " ORDER BY brands.name;";

        $results = $this->customQuery($query) ?? [];
        return $results;
    }

    public function getOfStar($filters = [])
    {
        $query = "SELECT products.rating, count(products.id) as total ";
        $query .= "FROM products ";

        if (count($filters)) {
            $where = $this->buildWhere($filters);
            $query .= " WHERE " . implode(" AND ", $where);
        }

        $query .= "GROUP BY products.rating ";

        $results = $this->customQuery($query) ?? [];
        return $results;
    }

    public function getSaleCount($filters = [])
    {
        $query = "SELECT count(products.id) as total ";
        $query .= "FROM products ";
        $query .= " WHERE products.sale = '1' ";

        if (count($filters)) {
            $where = $this->buildWhere($filters);
            $query .= "AND " . implode(" AND ", $where);
        }

        $result = $this->customQuery($query, false);

        if ($result) {
            return $result->total;
        }
        return 0;
    }

    public function getOfOptions($filters = [])
    {
        $groups = [];
        $ids = [];

        $query = "SELECT products.id, products.options ";
        $query .= "FROM products ";

        if (count($filters)) {
            $where = $this->buildWhere($filters);
            $query .= " WHERE " . implode(" AND ", $where);
        }

        $results = $this->customQuery($query) ?? [];

        foreach ($results as $result) {
            $ids[] = $result->id;
            $opts = explode(",", $result->options);
            foreach ($opts as $opt) {
                if (!in_array($opt, $groups)) {
                    $groups[] = $opt;
                }
            }
        }

        $options = $this->getAvailableOptions($groups, $ids);

        return $options;
    }

    private function getAvailableOptions($groups = array(), $ids = array())
    {
        $array = array();
        foreach ($groups as $group) {
            $option = (new Option())->findById($group);
            if ($option) {
                $array[$group] = [
                    "name" => $option->name,
                    "options" => array()
                ];
            }
        }

        $productOptions = (new ProductOption())->getCountOfOption($groups, $ids);
        foreach ($productOptions as $option) {
            $array[$option->option_id]["options"][] = [
                "option_id" => $option->option_id,
                "description" => $option->description,
                "total" => $option->total,
            ];
        }

        return $array;
    }

    private function buildWhere($filters = [])
    {
        $where = [];

        if (!empty($filters["product_id"])) {
            $where[] = "products.id = " . $filters["product_id"];
        }

        if (!empty($filters["brand"])) {
            $where[] = "products.brand_id IN ('".implode("','",$filters["brand"])."')";
        }

        if (!empty($filters["categories"])) {
            $where[] = "products.category_id IN ('".implode("','",$filters["categories"])."')";
        }

        if (!empty($filters["star"])) {
            $where[] = "products.rating IN ('".implode("','",$filters["star"])."')";
        }

        if (!empty($filters["sale"])) {
            $where[] = "products.sale = '1' ";
        }

        if (!empty($filters["featured"])) {
            $where[] = "products.featured = '1' ";
        }

        if (!empty($filters["options"])) {
            $where[] = "products.id IN (select po.product_id from product_options po where po.description IN ('".implode("','", $filters["options"])."')) ";
        }

        if (!empty($filters["slider0"])) {
            $where[] = "products.price >= {$filters["slider0"]} ";
        }

        if (!empty($filters["slider1"])) {
            $where[] = "products.price <= {$filters["slider1"]} ";
        }

        if (!empty($filters["searchTerm"])) {
            $where[] = "(products.name LIKE '%" . $filters["searchTerm"] . "%' OR products.description LIKE '%" . $filters["searchTerm"] . "%') ";
        }

        if (!empty($filters["searchCategory"])) {
            $where[] = "products.category_id  = " . $filters["searchCategory"];
        }

        return $where;
    }

    public function getCount($filters = [])
    {
        $query = "SELECT count(id) as total FROM products ";

        if (count($filters)) {
            $where = $this->buildWhere($filters);
            $query .= " WHERE " . implode(" AND ", $where);
        }

        $results = $this->customQuery($query);
        return $results[0]->total ?? 0;
    }

    public function getById($id, $columns = ["*"])
    {
        $model = $this->findById($id, $columns);

        if ($model) {
            $brand = (new Brand())->getById($model->brand_id, ["name"]);
            $category = (new Category())->getById($model->category_id, ["name"]);
            $model->brand = $brand->name;
            $model->category = $category->name;
            $model->images = (new ProductImage())->getByProduct($model->id);
            $model->productOptions = $this->getOfOptions(["product_id" => $model->id]);
            $model->productRates = (new ProductRate())->getByProduct($model->id, 5);
            return $model;
        }
        return null;
    }

    public function create(array $data)
    {
        $newData = $data;
        unset($newData["images"]);
        unset($newData["options"]);

        if (isset($data["options"]) && is_array($data["options"])) {
            $options = [];
            foreach ($data["options"] as $option_id => $description) {
                if (!empty($description)) {
                    $options[] = $option_id;
                }
            }
            $newData["options"] = implode(",", $options);
        }

        $modelId = $this->insert($newData);

        if ($modelId) {

            if (isset($data["options"]) && is_array($data["options"])) {
                (new ProductOption())->createSeveral($modelId, $data["options"]);
            }

            if (isset($data["images"]) && is_array($data["images"])) {
                (new ProductImage())->createSeveral($modelId, $data["images"]);
            }

            return $this->getById($modelId);
        }
        return null;
    }

    public function updateData($id, array $data)
    {
        $newData = $data;
        unset($newData["images"]);
        unset($newData["options"]);

        if (isset($data["options"]) && is_array($data["options"])) {
            $options = [];
            foreach ($data["options"] as $option_id => $description) {
                if (!empty($description)) {
                    $options[] = $option_id;
                }
            }
            $newData["options"] = implode(",", $options);
        }

        if ($this->update($newData, ["id" => $id])) {

            if (isset($data["options"]) && is_array($data["options"])) {
                (new ProductOption())->createSeveral($id, $data["options"]);
            }

            if (isset($data["images"]) && is_array($data["images"])) {
                (new ProductImage())->createSeveral($id, $data["images"]);
            }

            return $this->getById($id);
        }
        return false;
    }

    public function destroy($id)
    {
        return $this->update(["stock" => 0], ["id" => $id]);
    }

    public function forceDestroy($id)
    {
        $images = (new ProductImage())->read(true, ["*"], ["product_id" => $id]);
        foreach ($images as $image) {
            (new ProductImage())->delete(["id" => $image->id]);
        }
        $options = (new ProductOption())->read(true, ["*"], ["product_id" => $id]);
        foreach ($options as $option) {
            (new ProductOption())->delete(["id" => $option->id]);
        }
        $rates = (new ProductRate())->read(true, ["*"], ["product_id" => $id]);
        foreach ($rates as $rate) {
            (new ProductRate())->delete(["id" => $rate->id]);
        }
        $purchases = (new PurchaseProducts())->read(true, ["*"], ["product_id" => $id]);
        foreach ($purchases as $purchase) {
            (new PurchaseProducts())->delete(["id" => $purchase->id]);
        }
        return $this->delete(["id" => $id]);
    }
}

?>