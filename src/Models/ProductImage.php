<?php

namespace Src\Models;

use Src\Core\Model;

class ProductImage extends Model
{
    public function __construct()
    {
        parent::__construct("product_images");
    }

    public function all($where = [])
    {
        $results = $this->read(true, ["*"], $where) ?? [];
        return $results;
    }

    public function getById($id, $columns = ["*"])
    {
        $model = $this->findById($id, $columns);

        if ($model) {
            return $model;
        }
        return null;
    }

    public function getByProduct($product_id, $columns = ["*"])
    {
        $results = $this->read(true, ["*"], ["product_id" => $product_id]) ?? [];
        foreach ($results as $result) {
            $result->url = media("products/{$result->url}");
        }
        return $results;
    }

    public function createSeveral($product_id, array $data)
    {
        for($i = 0; $i < count($data['tmp_name']); $i++) {
            if(in_array($data['type'][$i], ["image/jpeg", "image/jpg", "image/png"])) {
                $type = $data["type"][$i];
                $tmpname = md5(time().rand(0,9999));
                $ext  = (in_array($type, ["image/jpeg", "image/jpg"]) ? ".jpg" : ".png");
                move_uploaded_file($data['tmp_name'][$i], 'media/products/'.$tmpname.$ext);

                $width = 460;
                $height = 400;
                $ratio = $width / $height;

                list($o_width, $o_height) = getimagesize('media/products/'. $tmpname.$ext);

                $o_ratio = $o_width / $o_height;

                if($ratio > $o_ratio) {
                    $img_w = $height * $o_ratio;
                    $img_h = $height;
                } else {
                    $img_h = $width / $o_ratio;
                    $img_w = $width;
                }

                if($img_w < $width) {
                    $img_w = $width;
                    $img_h = $img_w / $o_ratio;
                }
                if($img_h < $height) {
                    $img_h = $height;
                    $img_w = $img_h * $o_ratio;
                }

                $px = 0;
                $py = 0;

                if($img_w > $width) {
                    $px = ($img_w - $width) / 2;
                }

                if($img_h > $height) {
                    $py = ($img_h - $height) / 2;
                }

                $img = imagecreatetruecolor($width, $height);
                $original = null;
                if (in_array($type, ["image/jpeg", "image/jpg"])) {
                    $original = imagecreatefromjpeg('media/products/'.$tmpname.$ext);
                } else if (in_array($type, ["image/png"])) {
                    $original = imagecreatefrompng('media/products/'.$tmpname.$ext);
                }

                if ($original) {
                    imagecopyresampled($img, $original, -$px, -$py, 0, 0, $img_w, $img_h, $o_width, $o_height);
                    imagejpeg($img, 'media/products/'.$tmpname.$ext);

                    $insert = $this->insert([
                        "product_id" => $product_id,
                        "url" => $tmpname.$ext,
                    ]);

                    if (!$insert) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public function destroy($id)
    {
        return $this->delete(["id" => $id]);
    }
}

?>