<?php

namespace Src\Models;

use Src\Core\Model;

class Category extends Model
{
    public function __construct()
    {
        parent::__construct("categories");
    }

    public function all($where = [])
    {
        $this->order = "sub_id DESC";
        $results = $this->read(true, ["*"], $where) ?? [];

        $array = array();
        foreach ($results as $result) {
            $result->subs = [];
            $array[$result->id] = (array)$result;
        }

        while ($this->stillNeed($array)) {
            $this->organize($array);
        }

        return $array;
    }

    public function getTree($id)
    {
        $array = array();

        $haveChild = true;
        while ($haveChild) {
            $category = $this->getById($id);
            $array[] = $category;

            if (!empty($category->sub_id)) {
                $id = $category->sub_id;
            } else {
                $haveChild = false;
            }
        }

        return array_reverse($array);
    }

    public function getById($id, $columns = ["*"])
    {
        $model = $this->findById($id, $columns);

        if ($model) {
            return $model;
        }
        return null;
    }

    private function stillNeed($array)
    {
        foreach ($array as $item) {
            if (!empty($item["sub_id"])) {
                return true;
            }
        }
        return false;
    }

    private function organize(&$array)
    {
        foreach ($array as $id => $value) {
            if (isset($array[$value["sub_id"]])) {
                $array[$value["sub_id"]]["subs"][$value["id"]] = $value;
                unset($array[$id]);
                break;
            }
        }
    }

    public function create(array $data)
    {
        if (isset($data["sub_id"]) && empty($data["sub_id"])) {
            unset($data["sub_id"]);
        }

        $modelId = $this->insert($data);

        if ($modelId) {
            return $this->getById($modelId);
        }
        return null;
    }

    public function updateData($id, array $data)
    {
        if (isset($data["sub_id"]) && empty($data["sub_id"])) {
            unset($data["sub_id"]);
        }

        if ($this->update($data, ["id" => $id])) {
            return $this->getById($id);
        }
        return false;
    }

    public function destroy($id)
    {
        return $this->delete(["id" => $id]);
    }

    public function destroyAll($data = [])
    {
        foreach ($data as $item) {
            $this->delete(["id" => $item]);
        }
        return true;
    }

    public function scan($id, $data = [])
    {
        if (!in_array($id, $data)) {
            $data[] = $id;
        }

        $subs = $this->read(true, ["id"], ["sub_id" => $id]) ?? [];

        foreach ($subs as $item) {
            if (!in_array($item->id, $data)) {
                $data[] = $item->id;
            }

            $data = $this->scan($item->id, $data);
        }
        return $data;
    }

    public function hasProducts(array $data)
    {
        $count = (new Product())->getCount(["categories" => $data]);
        return $count;
    }
}

?>