<?php

namespace Src\Models;

use Src\Core\Model;

class ProductRate extends Model
{
    public function __construct()
    {
        parent::__construct("product_rates");
    }

    public function getById($id, $columns = ["*"])
    {
        $model = $this->findById($id, $columns);

        if ($model) {
            return $model;
        }
        return null;
    }

    public function getByProduct($product_id, $limit = null)
    {
        $results = $this->read(true, ["*"], ["product_id" => $product_id], [], $limit) ?? [];

        foreach ($results as $result) {
            $result->user = (new User())->getById($result->user_id);
        }
        return $results;
    }

    public function destroy($id, $product_id)
    {
        $delete = $this->delete(["id" => $id]);

        if ($delete) {
            $this->setRate($product_id);
            return true;
        }
        return false;
    }

    public function setRate($product_id)
    {
        $rates = $this->read(true, ["*"], ["product_id" => $product_id]) ?? [];
        $total = count($rates);
        $sum = 0;
        foreach ($rates as $rate) {
            $sum += intval($rate->points);
        }

        if ($sum > 0 && $total > 0) {
            $rating = ($sum / $total);
        } else {
            $rating = 0;
        }

        (new Product())->update(["rating" => $rating], ["id" => $product_id]);
        return true;
    }

    public function create(array $data)
    {
        $modelId = $this->insert($data);

        if ($modelId) {
            $this->setRate($data["product_id"]);
            return $this->getById($modelId);
        }
        return null;
    }
}

?>