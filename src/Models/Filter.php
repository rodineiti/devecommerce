<?php

namespace Src\Models;

class Filter
{
    public function get($filters = [])
    {
        $brands = (new Product())->getOfBrand($filters);
        $stars_products = (new Product())->getOfStar($filters);
        $maxValueSlider = (new Product())->getMaxPrice();
        $saleCount = (new Product())->getSaleCount($filters);
        $options = (new Product())->getOfOptions($filters);

        $array = [
            "brands" => $brands,
            "slider0" => $filters["slider0"] ?? 0,
            "slider1" => isset($filters["slider1"]) ? $filters["slider1"] : $maxValueSlider,
            "maxValueSlider" => $maxValueSlider,
            "stars" => [
                "0" => 0,
                "1" => 0,
                "2" => 0,
                "3" => 0,
                "4" => 0,
                "5" => 0,
            ],
            "sale" => $saleCount,
            "options" => $options,
            "searchTerm" => $filters["searchTerm"] ?? null,
            "searchCategory" => $filters["searchCategory"] ?? null,
        ];

        foreach ($array["stars"] as $key => $star) {
            foreach ($stars_products as $product) {
                if ($product->rating == $key) {
                    $array["stars"][$key] = $product->total;
                }
            }
        }

        return $array;
    }
}

?>