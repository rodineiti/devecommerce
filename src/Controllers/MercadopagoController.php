<?php

namespace Src\Controllers;

use Src\Core\Controller;
use Src\Models\Cart;
use Src\Models\Purchase;
use Src\Models\PurchaseProducts;
use Src\Models\Store;
use Src\Models\User;
use Src\Support\Mercadopago;
use Src\Support\Pagseguro;

class MercadopagoController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function transparent()
    {
        $data = (new Store())->getTemplateData();

        $data["sessionCode"] = Pagseguro::getSession();
        $data["total"] = number_format((new Cart())->total(), 2);

        $this->template("mercado_pago", $data);
    }

    public function payment()
    {
        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        $userExist = (new User())->exists("email", $request["email"]);

        if ($userExist) {
            $user = (new User())->attempt($request["email"], $request["password_rdn"]);

            if (!$user) {
                header("Location: " . BASE_URL . "error/index/1");
                exit;
            }
        } else {
            $user = (new User())->create([
                "name" => $request["name"],
                "email" => $request["email"],
                "password" => $request["password_rdn"],
            ]);
        }

        $purchaseId = (new Purchase())->create([
            "user_id" => $user->id,
            "amount" => (new Cart())->total(),
            "payment_type" => "mercado_pago",
        ]);

        if ($purchaseId) {

            $items = (new Cart())->get();

            foreach ($items as $value) {
                (new PurchaseProducts())->create([
                    "purchase_id" => $purchaseId,
                    "product_id" => $value["id"],
                    "qty" => $value["qty"],
                    "product_price" => $value["price"],
                ]);
            }

            $result = json_decode((new Mercadopago())->create($purchaseId, $request), true);

            if ($result["error"]) {
                header("Location: " . BASE_URL . "error/index/".$result["message"]);
                exit;
            }

            header("Location: " . $result["link_redirect"]);
            exit;
        }
    }

    public function success()
    {
        (new Cart())->destroy();
        $data = (new Store())->getTemplateData();
        $this->template("mercado_pago_success", $data);
    }

    public function pending()
    {
        (new Cart())->destroy();
        $data = (new Store())->getTemplateData();
        $this->template("mercado_pago_pending", $data);
    }

    public function failure()
    {
        (new Cart())->destroy();
        $data = (new Store())->getTemplateData();
        $this->template("mercado_pago_failure", $data);
    }

    public function notification($id)
    {
        $notification = json_decode((new Mercadopago())->notification($id), true);

        if ($notification["error"]) {
            $this->json([
                "error" => true,
                "message" => "Não foi possível processar a notificação, tente novamente.",
                "messageError" => $notification["message"]
            ]);
        }
    }
}