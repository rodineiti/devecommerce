<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Page;

class PagesController extends Controller
{
    protected $model;
    protected $redirect = "pages";
    protected $prefix = "pages";
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct("template_admin");
        $this->auth("admins");
        $this->model = new Page();
        $this->data = [];
        $this->required = ["title","content"];
    }

    public function index()
    {
        if (!hasPermission("{$this->prefix}-index")) {
            header("Location: " . back());
            exit;
        }

        $this->data["list"] = $this->model->all();
        $this->data["prefix"] = $this->prefix;
        $this->data["redirect"] = $this->redirect;
        $this->template("admin_page", $this->data);
    }

    public function create()
    {
        if (!hasPermission("{$this->prefix}-create")) {
            header("Location: " . back());
            exit;
        }

        $this->data["prefix"] = $this->prefix;
        $this->data["redirect"] = $this->redirect;
        $this->template("admin_page_create", $this->data);
    }

    public function store()
    {
        if (!hasPermission("{$this->prefix}-create")) {
            header("Location: " . back());
            exit;
        }

        $request = $this->request();

        if (!$this->required($request)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/create?error=fields");
            exit;
        }

        $model = $this->model->create($request);

        if (!$model) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/create?error=exists");
            exit;
        }

        header("Location: " . BASE_URL . "admin/{$this->redirect}/create?success");
        exit;
    }

    public function edit($id)
    {
        if (!hasPermission("{$this->prefix}-edit")) {
            header("Location: " . back());
            exit;
        }

        if (!$model = $this->model->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        $this->data["model"] = $model;
        $this->data["prefix"] = $this->prefix;
        $this->data["redirect"] = $this->redirect;
        $this->template("admin_page_edit", $this->data);
    }

    public function update($id)
    {
        if (!hasPermission("{$this->prefix}-edit")) {
            header("Location: " . back());
            exit;
        }

        if (!$this->model->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        $request = $this->request();

        if (!$this->required($request)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/create?error=fields");
            exit;
        }

        if (!$this->model->updateData($id, $request)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$id}?error=fields");
            exit;
        }

        header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$id}?success=edit");
        exit;
    }

    public function destroy($id)
    {
        if (!hasPermission("{$this->prefix}-destroy")) {
            header("Location: " . back());
            exit;
        }

        if (!$item = $this->model->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        $this->model->destroy($id);

        header("Location: " . BASE_URL . "admin/{$this->redirect}/index");
        exit;
    }

    public function upload()
    {
        if ($this->method() !== "POST") {
            $this->json(["error" => true, "message" => "Mothod not allowed"]);
        }

        if (isset($_FILES["file"]) && count($_FILES["file"])) {
            if (in_array($_FILES["file"]["type"], ["image/jpeg", "image/jpg", "image/png"])) {
                $tmpname = md5(time().rand(0,9999));
                $ext  = (in_array($_FILES["file"]["type"], ["image/jpeg", "image/jpg"]) ? ".jpg" : ".png");
                move_uploaded_file($_FILES["file"]['tmp_name'], 'media/pages/'.$tmpname.$ext);
                $this->json(["location" => media("pages/".$tmpname.$ext)]);
            }
        }
    }
}