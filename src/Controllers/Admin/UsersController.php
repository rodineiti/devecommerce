<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\User;

class UsersController extends Controller
{
    protected $user;
    protected $redirect = "users";
    protected $prefix = "users";

    public function __construct()
    {
        parent::__construct("template_admin");
        $this->auth("admins");
        $this->user = new User();
    }

    public function index()
    {
        if (!hasPermission("{$this->prefix}-index")) {
            header("Location: " . back());
            exit;
        }

        $data = array();
        $data["users"] = $this->user->all();
        $this->template("admin_user", $data);
    }

    public function create()
    {
        if (!hasPermission("{$this->prefix}-create")) {
            header("Location: " . back());
            exit;
        }

        $this->template("admin_user_create");
    }

    public function store()
    {
        if (!hasPermission("{$this->prefix}-create")) {
            header("Location: " . back());
            exit;
        }

        $data = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (empty($data["name"]) || empty($data["email"]) || empty($data["password"])) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/create?error=fields");
            exit;
        }

        $user = $this->user->create($data);

        if (!$user) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/create?error=exists");
            exit;
        }

        header("Location: " . BASE_URL . "admin/{$this->redirect}/create?success");
        exit;
    }

    public function edit($id)
    {
        if (!hasPermission("{$this->prefix}-edit")) {
            header("Location: " . back());
            exit;
        }

        if (!$user = $this->user->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        $data = array();
        $data["user"] = $user;
        $this->template("admin_user_edit", $data);
    }

    public function update($id)
    {
        if (!hasPermission("{$this->prefix}-edit")) {
            header("Location: " . back());
            exit;
        }

        $data = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (!$this->user->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        if (empty($data["name"]) || empty($data["email"])) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$id}?error=fields");
            exit;
        }

        if (!$this->user->updateProfile($id, $data)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$id}?error=fields");
            exit;
        }

        header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$id}?success=edit");
        exit;
    }

    public function destroy($id)
    {
        if (!hasPermission("{$this->prefix}-destroy")) {
            header("Location: " . back());
            exit;
        }

        if (!$user = $this->user->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        $this->user->destroy($user->id);

        header("Location: " . BASE_URL . "admin/{$this->redirect}/index");
        exit;
    }
}