<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Brand;
use Src\Models\Category;
use Src\Models\Option;
use Src\Models\Product;
use Src\Models\ProductImage;
use Src\Models\ProductOption;
use Src\Models\ProductRate;
use Src\Models\User;

class ProductsController extends Controller
{
    protected $model;
    protected $redirect = "products";
    protected $prefix = "products";
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct("template_admin");
        $this->auth("admins");
        $this->model = new Product();
        $this->data = [];
        $this->required = [
            "category_id", "brand_id", "name", "description", "stock",
            "price_from", "price", "weight", "width", "height","length", "diameter"
        ];
    }

    public function index()
    {
        if (!hasPermission("{$this->prefix}-index")) {
            header("Location: " . back());
            exit;
        }

        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);
        $limit = $this->limit ?? 10;
        $page = !empty($request["page"]) ? intval($request["page"]) : 1;
        $offset = (($page * $limit) - $limit);

        $products = $this->model->all($offset, $limit);
        $productsCount = $this->model->getCount();
        $pages = ceil($productsCount / $limit);

        $this->data["list"] = $products;
        $this->data["total"] = count($products);
        $this->data["pages"] = $pages;
        $this->data["page"] = $page;
        $this->data["prefix"] = $this->prefix;
        $this->data["redirect"] = $this->redirect;
        $this->template("admin_product", $this->data);
    }

    public function create()
    {
        if (!hasPermission("{$this->prefix}-create")) {
            header("Location: " . back());
            exit;
        }

        $this->data["categories"] = (new Category())->all();
        $this->data["brands"] = (new Brand())->all();
        $this->data["options"] = (new Option())->all();
        $this->data["prefix"] = $this->prefix;
        $this->data["redirect"] = $this->redirect;
        $this->template("admin_product_create", $this->data);
    }

    public function store()
    {
        if (!hasPermission("{$this->prefix}-create")) {
            header("Location: " . back());
            exit;
        }

        $description = $this->request()["description"] ?? "";
        $files = (isset($_FILES["images"]) &&count($_FILES["images"])) ? $_FILES["images"] : null;
        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);
        $request["description"] = $description;
        $request["images"] = $files;

        if (!$this->required($request)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/create?error=fields");
            exit;
        }

        $model = $this->model->create($request);

        if (!$model) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/create?error=exists");
            exit;
        }

        header("Location: " . BASE_URL . "admin/{$this->redirect}/create?success");
        exit;
    }

    public function edit($id)
    {
        if (!hasPermission("{$this->prefix}-edit")) {
            header("Location: " . back());
            exit;
        }

        if (!$model = $this->model->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        $optionsProduct = (new ProductOption())->getByProduct($model->id);
        $imagesProduct = (new ProductImage())->getByProduct($model->id);

        $this->data["model"] = $model;
        $this->data["categories"] = (new Category())->all();
        $this->data["brands"] = (new Brand())->all();
        $this->data["options"] = (new Option())->all();
        $this->data["users"] = (new User())->all();
        $this->data["optionsProduct"] = $optionsProduct;
        $this->data["imagesProduct"] = $imagesProduct;
        $this->data["prefix"] = $this->prefix;
        $this->data["redirect"] = $this->redirect;
        $this->template("admin_product_edit", $this->data);
    }

    public function update($id)
    {
        if (!hasPermission("{$this->prefix}-edit")) {
            header("Location: " . back());
            exit;
        }

        if (!$this->model->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        $description = $this->request()["description"] ?? "";
        $files = (isset($_FILES["images"]) &&count($_FILES["images"])) ? $_FILES["images"] : null;
        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);
        $request["description"] = $description;
        $request["images"] = $files;

        if (!$this->required($request)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$id}?error=fields");
            exit;
        }

        if (!$this->model->updateData($id, $request)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$id}?error=fields");
            exit;
        }

        header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$id}?success=edit");
        exit;
    }

    public function destroy($id)
    {
        if (!hasPermission("{$this->prefix}-destroy")) {
            header("Location: " . back());
            exit;
        }

        if (!$item = $this->model->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        $this->model->destroy($id);

        header("Location: " . BASE_URL . "admin/{$this->redirect}/index");
        exit;
    }

    public function delete_image($product_id, $id)
    {
        if (!$model = $this->model->getById($product_id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$product_id}?error=fields");
            exit;
        }

        if (!$image = (new ProductImage())->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$product_id}?error=fields");
            exit;
        }

        (new ProductImage())->destroy($image->id);

        header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$model->id}?success=edit");
        exit;
    }

    public function delete_rate($product_id, $id)
    {
        if (!$model = $this->model->getById($product_id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$product_id}?error=fields");
            exit;
        }

        if (!$rate = (new ProductRate())->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$product_id}?error=fields");
            exit;
        }

        (new ProductRate())->destroy($rate->id, $model->id);

        header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$model->id}?success=edit");
        exit;
    }

    public function add_rate($product_id)
    {
        if (!hasPermission("{$this->prefix}-edit")) {
            header("Location: " . back());
            exit;
        }

        if (!$model = $this->model->getById($product_id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (empty($request["user_id"]) || empty($request["points"])) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$model->id}?error=fields");
            exit;
        }

        $request["product_id"] = $model->id;

        if (!(new ProductRate())->create($request)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$model->id}?error=fields");
            exit;
        }

        header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$model->id}?success=edit");
        exit;
    }
}