<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Option;
use Src\Models\ProductOption;

class OptionsController extends Controller
{
    protected $model;
    protected $redirect = "options";
    protected $prefix = "products";
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct("template_admin");
        $this->auth("admins");
        $this->model = new Option();
        $this->data = [];
        $this->required = ["name"];
    }

    public function index()
    {
        if (!hasPermission("{$this->prefix}-index")) {
            header("Location: " . back());
            exit;
        }

        $this->data["list"] = $this->model->all();
        $this->data["prefix"] = $this->prefix;
        $this->data["redirect"] = $this->redirect;
        $this->template("admin_option", $this->data);
    }

    public function create()
    {
        if (!hasPermission("{$this->prefix}-create")) {
            header("Location: " . back());
            exit;
        }

        $this->data["prefix"] = $this->prefix;
        $this->data["redirect"] = $this->redirect;
        $this->template("admin_option_create", $this->data);
    }

    public function store()
    {
        if (!hasPermission("{$this->prefix}-create")) {
            header("Location: " . back());
            exit;
        }

        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (!$this->required($request)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/create?error=fields");
            exit;
        }

        $model = $this->model->create($request);

        if (!$model) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/create?error=exists");
            exit;
        }

        header("Location: " . BASE_URL . "admin/{$this->redirect}/create?success");
        exit;
    }

    public function edit($id)
    {
        if (!hasPermission("{$this->prefix}-edit")) {
            header("Location: " . back());
            exit;
        }

        if (!$model = $this->model->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        $this->data["model"] = $model;
        $this->data["prefix"] = $this->prefix;
        $this->data["redirect"] = $this->redirect;
        $this->template("admin_option_edit", $this->data);
    }

    public function update($id)
    {
        if (!hasPermission("{$this->prefix}-edit")) {
            header("Location: " . back());
            exit;
        }

        if (!$this->model->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (!$this->required($request)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/create?error=fields");
            exit;
        }

        if (!$this->model->updateData($id, $request)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$id}?error=fields");
            exit;
        }

        header("Location: " . BASE_URL . "admin/{$this->redirect}/edit/{$id}?success=edit");
        exit;
    }

    public function destroy($id)
    {
        if (!hasPermission("{$this->prefix}-destroy")) {
            header("Location: " . back());
            exit;
        }

        if (!$item = $this->model->getById($id)) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        $checkOption = (new ProductOption())->getByOption($id);

        // não permite deletar se houver vinculo de opções
        if (count($checkOption) > 0) {
            header("Location: " . BASE_URL . "admin/{$this->redirect}/index?error");
            exit;
        }

        $this->model->destroy($item->id);

        header("Location: " . BASE_URL . "admin/{$this->redirect}/index");
        exit;
    }
}