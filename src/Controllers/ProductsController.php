<?php

namespace Src\Controllers;

use Src\Core\Controller;
use Src\Models\Product;
use Src\Models\Store;

class ProductsController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index($id)
    {
        header("Location: " . BASE_URL);
        exit;
    }

    public function show($id)
    {
        $data = (new Store())->getTemplateData();
        $product = (new Product())->getById($id);

        if (!$product) {
            header("Location: " . BASE_URL);
            exit;
        }

        $data["product"] = $product;

        $this->template("product", $data);
    }
}