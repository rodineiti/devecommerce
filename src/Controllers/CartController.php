<?php

namespace Src\Controllers;

use Src\Core\Controller;
use Src\Models\Cart;
use Src\Models\Product;
use Src\Models\Store;

class CartController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if (!isset($_SESSION["cart"]) || (isset($_SESSION["cart"]) && count($_SESSION["cart"]) === 0)) {
            header("Location: " . BASE_URL);
            exit;
        }

        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);
        $data = (new Store())->getTemplateData();
        $shipping = array();

        if (!empty($request["cep"])) {
            $shipping = (new Cart())->calculateShipping($request["cep"]);
        }

        if (!empty($_SESSION["shipping"])) {
            $shipping = $_SESSION["shipping"];
        }

        $data["products_cart"] = (new Cart())->get();
        $data["products_subtotal"] = (new Cart())->subtotal();
        $data["shipping"] = $shipping;

        $this->template("cart", $data);
    }

    public function add($product_id)
    {
        $product_qty = filter_var($this->request()["product_qty"], FILTER_VALIDATE_INT);

        $product = (new Product())->getById($product_id);

        if (!$product) {
            header("Location: " . BASE_URL);
            exit;
        }

        (new Cart())->add($product, $product_qty);

        header("Location: " . BASE_URL . "cart");
        exit;
    }

    public function del($product_id)
    {
        $product = (new Product())->getById($product_id);

        if (!$product) {
            header("Location: " . BASE_URL);
            exit;
        }

        (new Cart())->del($product);

        header("Location: " . BASE_URL . "cart");
        exit;
    }

    public function inc($product_id)
    {
        $product = (new Product())->getById($product_id);

        if (!$product) {
            header("Location: " . BASE_URL);
            exit;
        }

        (new Cart())->inc($product);

        header("Location: " . BASE_URL . "cart");
        exit;
    }

    public function dec($product_id)
    {
        $product = (new Product())->getById($product_id);

        if (!$product) {
            header("Location: " . BASE_URL);
            exit;
        }

        (new Cart())->dec($product);

        header("Location: " . BASE_URL . "cart");
        exit;
    }

    public function payment_redirect()
    {
        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (empty($request["payment_type"])) {
            header("Location: " . BASE_URL . "cart");
            exit;
        }

        switch ($request["payment_type"]) {
            case "pagseguro_checkout_transparent":
                header("Location: " . BASE_URL . "pagseguro/transparent");
                exit;
                break;
            case "mercadopago_checkout":
                header("Location: " . BASE_URL . "mercadopago/transparent");
                exit;
                break;
            case "paypal_checkout":
                header("Location: " . BASE_URL . "paypal/transparent");
                exit;
                break;
            case "boleto":
                header("Location: " . BASE_URL . "boleto");
                exit;
                break;
            default:
                header("Location: " . BASE_URL . "cart");
                exit;
                break;
        }
    }
}