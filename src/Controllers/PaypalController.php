<?php

namespace Src\Controllers;

use Src\Core\Controller;
use Src\Models\Cart;
use Src\Models\Purchase;
use Src\Models\PurchaseProducts;
use Src\Models\Store;
use Src\Models\User;
use Src\Support\Paypal;

class PaypalController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function transparent()
    {
        $data = (new Store())->getTemplateData();
        $this->template("paypal", $data);
    }

    public function payment()
    {
        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        $userExist = (new User())->exists("email", $request["email"]);

        if ($userExist) {
            $user = (new User())->attempt($request["email"], $request["password_rdn"]);

            if (!$user) {
                header("Location: " . BASE_URL . "error/index/1");
                exit;
            }
        } else {
            $user = (new User())->create([
                "name" => $request["name"],
                "email" => $request["email"],
                "password" => $request["password_rdn"],
            ]);
        }

        $purchaseId = (new Purchase())->create([
            "user_id" => $user->id,
            "amount" => (new Cart())->total(),
            "payment_type" => "paypal",
        ]);

        if ($purchaseId) {

            $items = (new Cart())->get();

            foreach ($items as $value) {
                (new PurchaseProducts())->create([
                    "purchase_id" => $purchaseId,
                    "product_id" => $value["id"],
                    "qty" => $value["qty"],
                    "product_price" => $value["price"],
                ]);
            }

            $result = json_decode((new Paypal())->create($purchaseId, $request), true);

            if ($result["error"]) {
                header("Location: " . BASE_URL . "error/index/2");
                exit;
            }

            header("Location: " . $result["link_redirect"]);
            exit;
        }
    }

    public function success()
    {
        $paymentId = $this->request()["paymentId"];
        $payerID = $this->request()["PayerID"];
        $data = (new Store())->getTemplateData();
        (new Paypal())->notification($paymentId, $payerID);
        $this->template("paypal_success", $data);
    }

    public function cancel()
    {
        $data = (new Store())->getTemplateData();
        $this->template("paypal_cancel", $data);
    }
}