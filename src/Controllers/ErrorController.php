<?php

namespace Src\Controllers;

use Src\Core\Controller;
use Src\Models\Store;

class ErrorController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index($id)
    {
        $data = (new Store())->getTemplateData();
        $data["status"] = "danger";
        $data["error"] = is_numeric($id) ? getError($id) : $id;
        $this->template("error", $data);
    }
}