<?php

namespace Src\Controllers;

use Src\Core\Controller;

class LangController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index($lang)
    {
        $_SESSION["lang"] = $lang;
        header("Location: " . BASE_URL);
    }
}