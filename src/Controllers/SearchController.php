<?php

namespace Src\Controllers;

use Src\Core\Controller;
use Src\Models\Filter;
use Src\Models\Product;
use Src\Models\Store;

class SearchController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = (new Store())->getTemplateData();
        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);
        $limit = $this->limit ?? 10;
        $page = !empty($request["page"]) ? intval($request["page"]) : 1;
        $offset = (($page * $limit) - $limit);
        $filters = (!empty($request["filter"]) && is_array($request["filter"])) ? $request["filter"] : [];
        $searchTerm = (!empty($request["s"]) ? $request["s"] : null);
        if ($searchTerm) {
            $filters["searchTerm"] = $searchTerm;
        }

        $searchCategory = (!empty($request["category"]) ? $request["category"] : null);
        if ($searchCategory) {
            $filters["searchCategory"] = $searchCategory;
        }

        $products = (new Product())->all($offset, $limit, $filters);
        $productsCount = (new Product())->getCount($filters);

        $pages = ceil($productsCount / $limit);

        $data["products"] = $products;
        $data["total"] = count($products);
        $data["pages"] = $pages;
        $data["page"] = $page;
        $data["filters"] = (new Filter())->get($filters);
        $data["filter_selected"] = $filters;
        $data["searchTerm"] = $searchTerm;
        $data["searchCategory"] = $searchCategory;

        $data["sidebar"] = true;

        $this->template("search", $data);
    }
}