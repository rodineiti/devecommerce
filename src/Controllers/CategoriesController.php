<?php

namespace Src\Controllers;

use Src\Core\Controller;
use Src\Models\Category;
use Src\Models\Filter;
use Src\Models\Product;
use Src\Models\Store;

class CategoriesController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        header("Location: " . BASE_URL);
        exit;
    }

    public function show($id)
    {
        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);
        $data = (new Store())->getTemplateData();

        $category_filter = (new Category())->getById($id);

        if (!$category_filter) {
            header("Location: " . BASE_URL);
            exit;
        }

        $limit = $this->limit ?? 10;
        $page = !empty($request["page"]) ? intval($request["page"]) : 1;
        $offset = (($page * $limit) - $limit);
        $filters = ["searchCategory" => $category_filter->id];

        $products = (new Product())->all($offset, $limit, $filters);
        $productsCount = (new Product())->getCount($filters);

        $pages = ceil($productsCount / $limit);

        $data["products"] = $products;
        $data["total"] = count($products);
        $data["pages"] = $pages;
        $data["page"] = $page;
        $data["category_filter"] = $category_filter;
        $data["categories_filter"] = (new Category())->getTree($category_filter->id);
        $data["filters"] = (new Filter())->get($filters);
        $data["filter_selected"] = $filters;
        $data["sidebar"] = true;

        $this->template("categories", $data);
    }
}