<?php

namespace Src\Controllers;

use Src\Core\Controller;
use Src\Models\Cart;
use Src\Models\Purchase;
use Src\Models\PurchaseProducts;
use Src\Models\User;
use Src\Support\Pagseguro;

class AjaxController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function pagseguro_checkout()
    {
        if ($this->method() !== "POST") {
            $this->json(["error" => true, "message" => "Method not allowed"]);
        }

        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        $userExist = (new User())->exists("email", $request["email"]);

        if ($userExist) {
            $user = (new User())->attempt($request["email"], $request["password_rdn"]);

            if (!$user) {
                $this->json(["error" => true, "message" => "E-mail  e/ou senha inválidos"]);
            }
        } else {
            $user = (new User())->create([
                "name" => $request["name"],
                "email" => $request["email"],
                "password" => $request["password_rdn"],
            ]);
        }

        $purchaseId = (new Purchase())->create([
            "user_id" => $user->id,
            "amount" => (new Cart())->total(),
            "payment_type" => "pagseguro_transparent",
        ]);

        if ($purchaseId) {

            $items = (new Cart())->get();

            foreach ($items as $value) {
                (new PurchaseProducts())->create([
                    "purchase_id" => $purchaseId,
                    "product_id" => $value["id"],
                    "qty" => $value["qty"],
                    "product_price" => $value["price"],
                ]);
            }

            $result = json_decode(Pagseguro::creditCard($purchaseId, $request), true);

            if ($result["error"]) {
                $this->json([
                    "error" => true,
                    "message" => "Não foi possível processar o pagamento, tente novamente.",
                    "messageError" => $result["message"]
                ]);
            }

            $this->json(["error" => false, "message" => "Pagamento efetuado com sucesso.", "data" => $result]);
        }
    }
}