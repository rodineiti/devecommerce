<?php

namespace Src\Controllers;

use Src\Core\Controller;
use Src\Models\Cart;
use Src\Models\Store;
use Src\Support\Pagseguro;

class PagseguroController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function transparent()
    {
        $data = (new Store())->getTemplateData();

        $data["sessionCode"] = Pagseguro::getSession();
        $data["total"] = number_format((new Cart())->total(), 2);

        $this->template("pagseguro", $data);
    }

    public function success()
    {
        (new Cart())->destroy();
        $data = (new Store())->getTemplateData();
        $this->template("pagseguro_success", $data);
    }

    public function notification()
    {
        $notification = json_decode(Pagseguro::hasPost(), true);

        if ($notification["error"]) {
            $this->json([
                "error" => true,
                "message" => "Não foi possível processar a notificação, tente novamente.",
                "messageError" => $notification["message"]
            ]);
        }
    }
}