<?php

namespace Src\Controllers;

use Src\Core\Controller;
use Src\Models\Store;

class NotfoundController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = (new Store())->getTemplateData();
        $this->template("404", $data);
    }
}