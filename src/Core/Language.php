<?php

namespace Src\Core;

class Language
{
    private $lang;
    private $ini;

    public function __construct()
    {
        $this->lang = CONF_DEFAULT_LANG;

        if (!empty($_SESSION["lang"]) && file_exists("lang/".$_SESSION["lang"].".ini")) {
            $this->lang = $_SESSION["lang"];
        }

        $this->ini = parse_ini_file("lang/".$this->lang.".ini");
    }

    public function get($word, $return = false)
    {
        if (!$return) {
            echo $this->ini[$word] ?? $word;
        }

        return $this->ini[$word] ?? $word;
    }
}