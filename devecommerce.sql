-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: devecommerce
-- ------------------------------------------------------
-- Server version	5.7.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admins` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `permission_group_id` int(11) NOT NULL DEFAULT '2',
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  FULLTEXT KEY `name_email_FULLTEXT` (`name`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (1,1,'admin','admin@admin.com','$2y$10$JxsGcZCAPf6pl8BvnppKtONxdVxkJW1pKjWzlr67oSBFS5/nD/oVm','2020-08-04 22:03:21','2020-08-05 17:22:19');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `brands` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `name_FULLTEXT` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` VALUES (1,'LG','2020-07-27 15:50:16',NULL),(2,'Samsung','2020-07-27 15:50:16',NULL),(3,'AOC','2020-07-27 15:50:16',NULL),(4,'Apple','2020-07-28 15:49:30',NULL);
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sub_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `name_FULLTEXT` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,NULL,'Monitor','2020-07-27 15:50:30',NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cupons`
--

DROP TABLE IF EXISTS `cupons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cupons` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` int(11) DEFAULT '0',
  `cupom_value` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cupons`
--

LOCK TABLES `cupons` WRITE;
/*!40000 ALTER TABLE `cupons` DISABLE KEYS */;
/*!40000 ALTER TABLE `cupons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `options`
--

DROP TABLE IF EXISTS `options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `options` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `name_FULLTEXT` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `options`
--

LOCK TABLES `options` WRITE;
/*!40000 ALTER TABLE `options` DISABLE KEYS */;
INSERT INTO `options` VALUES (1,'Cor','2020-07-28 16:46:11',NULL),(2,'Tamanho','2020-07-28 16:46:11',NULL),(3,'Memória RAM','2020-07-28 16:46:11','2020-07-28 19:32:02'),(4,'Polegadas','2020-07-28 16:46:11','2020-07-28 19:32:02');
/*!40000 ALTER TABLE `options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `content_FULLTEXT` (`content`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_group_items`
--

DROP TABLE IF EXISTS `permission_group_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permission_group_items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `permission_group_id` int(11) NOT NULL,
  `permission_item_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_group_items`
--

LOCK TABLES `permission_group_items` WRITE;
/*!40000 ALTER TABLE `permission_group_items` DISABLE KEYS */;
INSERT INTO `permission_group_items` VALUES (67,1,1,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(68,1,2,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(69,1,3,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(70,1,4,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(71,1,5,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(72,1,6,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(73,1,7,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(74,1,8,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(75,1,9,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(76,1,10,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(77,1,11,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(78,1,13,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(79,1,14,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(80,1,15,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(81,1,16,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(82,1,17,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(83,1,18,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(84,1,19,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(85,1,20,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(86,1,21,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(87,1,22,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(88,1,23,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(89,1,24,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(90,1,25,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(91,1,26,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(92,1,27,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(93,1,28,'2020-08-06 18:57:05','2020-08-06 18:57:05'),(94,1,29,'2020-08-06 18:57:05','2020-08-06 18:57:05');
/*!40000 ALTER TABLE `permission_group_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_groups`
--

DROP TABLE IF EXISTS `permission_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permission_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_groups`
--

LOCK TABLES `permission_groups` WRITE;
/*!40000 ALTER TABLE `permission_groups` DISABLE KEYS */;
INSERT INTO `permission_groups` VALUES (1,'Super Administrador','2020-08-05 16:32:32','2020-08-05 16:32:32'),(7,'Administrador','2020-08-05 22:01:27','2020-08-05 22:01:27');
/*!40000 ALTER TABLE `permission_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_items`
--

DROP TABLE IF EXISTS `permission_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permission_items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_items`
--

LOCK TABLES `permission_items` WRITE;
/*!40000 ALTER TABLE `permission_items` DISABLE KEYS */;
INSERT INTO `permission_items` VALUES (1,'Listar Usuários','users-index','2020-08-05 21:05:20','2020-08-05 22:42:20'),(2,'Criar Usuários','users-create','2020-08-05 21:05:20','2020-08-05 21:05:20'),(3,'Editar Usuários','users-edit','2020-08-05 21:05:20','2020-08-05 21:05:20'),(4,'Deletar Usuários','users-destroy','2020-08-05 21:05:20','2020-08-05 21:05:20'),(5,'Listar Grupo de Permissões','permission-groups-index','2020-08-05 21:05:20','2020-08-05 21:05:20'),(6,'Criar Grupo de Permissões','permission-groups-create','2020-08-05 21:05:20','2020-08-05 21:05:20'),(7,'Editar Grupo de Permissões','permission-groups-edit','2020-08-05 21:05:20','2020-08-05 21:05:20'),(8,'Deletar Grupo de Permissões','permission-groups-destroy','2020-08-05 21:05:20','2020-08-05 21:05:20'),(9,'Listar Item de Permissões','permission-items-index','2020-08-05 22:36:33','2020-08-05 22:36:33'),(10,'Criar Item de Permissões','permission-items-create','2020-08-05 22:36:33','2020-08-05 22:36:33'),(11,'Editar Item de Permissões','permission-items-edit','2020-08-05 22:36:33','2020-08-05 22:36:33'),(13,'Deletar Item de Permissões','permission-items-destroy','2020-08-05 22:46:17','2020-08-05 22:46:17'),(14,'Listar Marcas','brands-index','2020-08-06 15:04:32','2020-08-06 15:04:32'),(15,'Criar Marcas','brands-create','2020-08-06 15:04:32','2020-08-06 15:04:32'),(16,'Editar Marcas','brands-edit','2020-08-06 15:04:32','2020-08-06 15:04:32'),(17,'Deletar Marcas','brands-destroy','2020-08-06 15:04:32','2020-08-06 15:04:32'),(18,'Listar Categorias','categories-index','2020-08-06 15:04:32','2020-08-06 15:04:32'),(19,'Criar Categorias','categories-create','2020-08-06 15:04:32','2020-08-06 15:04:32'),(20,'Editar Categorias','categories-edit','2020-08-06 15:04:32','2020-08-06 15:04:32'),(21,'Deletar Categorias','categories-destroy','2020-08-06 15:04:32','2020-08-06 15:04:32'),(22,'Listar Produtos','products-index','2020-08-06 15:04:32','2020-08-06 15:04:32'),(23,'Criar Produtos','products-create','2020-08-06 15:04:32','2020-08-06 15:04:32'),(24,'Editar Produtos','products-edit','2020-08-06 15:04:32','2020-08-06 15:04:32'),(25,'Deletar Produtos','products-destroy','2020-08-06 15:04:32','2020-08-06 15:04:32'),(26,'Listar Páginas','pages-index','2020-08-06 18:55:56','2020-08-06 18:55:56'),(27,'Criar Páginas','pages-create','2020-08-06 18:55:56','2020-08-06 18:55:56'),(28,'Editar Páginas','pages-edit','2020-08-06 18:55:56','2020-08-06 18:55:56'),(29,'Deletar Páginas','pages-destroy','2020-08-06 18:55:56','2020-08-06 18:55:56');
/*!40000 ALTER TABLE `permission_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_images`
--

DROP TABLE IF EXISTS `product_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_images` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_images`
--

LOCK TABLES `product_images` WRITE;
/*!40000 ALTER TABLE `product_images` DISABLE KEYS */;
INSERT INTO `product_images` VALUES (1,1,'1.jpg','2020-07-27 15:56:29',NULL),(2,2,'2.jpg','2020-07-27 15:56:29',NULL),(3,3,'3.jpg','2020-07-27 15:56:29',NULL),(4,4,'4.jpg','2020-07-27 15:56:29',NULL),(5,5,'5.jpg','2020-07-27 15:56:29',NULL),(6,6,'6.jpg','2020-07-27 15:56:29',NULL),(7,7,'7.jpg','2020-07-27 15:56:29',NULL),(8,8,'8.jpg','2020-07-27 15:56:29',NULL),(11,2,'3.jpg','2020-07-30 16:11:32',NULL),(12,2,'4.jpg','2020-07-30 16:11:32',NULL),(13,2,'5.jpg','2020-07-30 16:11:32',NULL),(14,13,'3395874e3714a29a8ed868fda93643f1.jpg','2020-08-07 16:14:03',NULL),(15,13,'b62cbba2f5dc4a9b5901d7b20be6236a.jpg','2020-08-07 16:14:03',NULL);
/*!40000 ALTER TABLE `product_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_options`
--

DROP TABLE IF EXISTS `product_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_options` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `name_FULLTEXT` (`description`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_options`
--

LOCK TABLES `product_options` WRITE;
/*!40000 ALTER TABLE `product_options` DISABLE KEYS */;
INSERT INTO `product_options` VALUES (1,1,1,'Azul','2020-07-28 16:48:29',NULL),(2,1,2,'23CM','2020-07-28 16:48:29',NULL),(3,1,4,'21','2020-07-28 16:48:29','2020-07-28 16:48:45'),(4,2,1,'Azul','2020-07-28 16:48:29',NULL),(5,2,2,'19cm','2020-07-28 16:48:29',NULL),(6,3,1,'Vermelho','2020-07-28 19:33:37',NULL),(7,3,2,'19cm','2020-07-28 19:33:37',NULL),(8,9,1,'cor','2020-08-07 15:32:26',NULL),(9,9,2,'tamanho','2020-08-07 15:32:26',NULL),(10,9,3,'memoria','2020-08-07 15:32:26',NULL),(11,9,4,'polegadas','2020-08-07 15:32:26',NULL),(12,10,1,'cor','2020-08-07 15:38:26',NULL),(13,10,2,'tamanho','2020-08-07 15:38:26',NULL),(14,10,3,'memoria','2020-08-07 15:38:26',NULL),(15,10,4,'polegadas','2020-08-07 15:38:26',NULL),(16,11,1,'cor oac','2020-08-07 15:39:20',NULL),(17,12,1,'cor img','2020-08-07 16:12:01',NULL),(18,12,2,'60','2020-08-07 16:12:01',NULL),(19,12,4,'50','2020-08-07 16:12:01',NULL);
/*!40000 ALTER TABLE `product_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_rates`
--

DROP TABLE IF EXISTS `product_rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_rates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  `description` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `name_FULLTEXT` (`description`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_rates`
--

LOCK TABLES `product_rates` WRITE;
/*!40000 ALTER TABLE `product_rates` DISABLE KEYS */;
INSERT INTO `product_rates` VALUES (1,1,2,2,'Produto legal','2020-07-30 16:59:31',NULL),(2,1,2,2,'Interessante','2020-07-30 17:00:30',NULL);
/*!40000 ALTER TABLE `product_rates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL COMMENT 'marca',
  `name` varchar(255) NOT NULL,
  `description` text,
  `stock` int(11) NOT NULL COMMENT 'quantidade em estoque',
  `price` float DEFAULT '0' COMMENT 'preco atual',
  `price_from` float DEFAULT '0' COMMENT 'preco anterior',
  `rating` float DEFAULT '0' COMMENT 'pra hankear avaliados, guardar a media',
  `featured` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'pra hankear destaques',
  `sale` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'se está em promoção',
  `bestseller` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'se está com muita venda',
  `new_add` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'novo produto',
  `options` varchar(255) DEFAULT NULL COMMENT 'optional utilizar pois irei utilizar uma tabela para relacionar',
  `weight` float DEFAULT '0',
  `width` float DEFAULT '0',
  `height` float DEFAULT '0',
  `length` float DEFAULT '0',
  `diameter` float DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `name_description_FULLTEXT` (`name`,`description`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,1,1,'Monitor 21 polegadas','descrição do produto',10,499,599,0,0,1,0,0,'1,2,4',0.9,20,15,20,15,'2020-07-27 15:54:28','2020-07-31 18:56:16'),(2,1,2,'Monitor 18 polegadas','descrição',10,399,999,2,0,0,1,0,'1,2',0.8,20,15,20,15,'2020-07-27 15:54:28','2020-07-31 18:56:16'),(3,1,2,'Monitor 19','descrição',10,599,699,0,1,0,0,1,'1,2',0.7,20,15,20,15,'2020-07-27 15:54:28','2020-07-31 18:56:16'),(4,1,3,'Monitor 17','descrição',10,779,900,2,1,1,0,0,'1,4',0.6,20,15,20,15,'2020-07-27 15:54:28','2020-07-31 18:56:16'),(5,1,1,'Monitor 20','descrição',10,299,499,0,1,1,1,0,'1',0.5,20,15,20,15,'2020-07-27 15:54:28','2020-07-31 18:56:16'),(6,1,3,'Monitor 20','descrição',10,699,0,0,1,0,0,0,'1,2,4',0.4,20,15,20,15,'2020-07-27 15:54:28','2020-07-31 18:56:16'),(7,1,3,'Monitor 19','descrição',10,889,999,5,1,1,1,1,'2,4',0.3,20,15,20,15,'2020-07-27 15:54:28','2020-07-31 18:56:16'),(8,1,1,'Monitor 18','descrição',10,599,699,0,0,0,0,0,'4',0.2,20,15,20,15,'2020-07-27 15:54:28','2020-07-31 18:56:16'),(9,1,1,'Monitor LG','<p>Monitor LG</p>',1,1,1,0,1,1,1,1,NULL,1,1,1,1,1,'2020-08-07 15:32:26',NULL),(10,1,2,'Monitor Samsung Rd','<p>Monitor Samsung Rd</p>',1,1,10,0,1,1,1,1,'1,2,3,4',1,1,1,1,1,'2020-08-07 15:38:26',NULL),(11,1,3,'AOC','<p>AOC</p>',1,1,1,0,0,0,0,0,'1',1,1,1,1,1,'2020-08-07 15:39:20',NULL),(12,1,1,'Monitor com image LG','<p>Monitor com image LG</p>',1,1,1,0,0,0,0,0,'1,2,4',1,1,1,1,1,'2020-08-07 16:12:01',NULL),(13,1,2,'Monitor Samsung imagens','<p>Monitor Samsung imagens</p>',1,1,1,0,0,0,0,0,'',1,1,1,1,1,'2020-08-07 16:14:03',NULL);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchases`
--

DROP TABLE IF EXISTS `purchases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchases` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `cupom_id` int(11) DEFAULT NULL,
  `amount` float NOT NULL,
  `payment_type` varchar(255) DEFAULT NULL,
  `payment_status` int(11) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchases`
--

LOCK TABLES `purchases` WRITE;
/*!40000 ALTER TABLE `purchases` DISABLE KEYS */;
INSERT INTO `purchases` VALUES (20,2,NULL,499,'paypal',1,'https://visualizacaosandbox.gerencianet.com.br/emissao/259397_2_BRAMA1/A4XB-259397-2-LUAZE7','2020-08-06 14:18:49','2020-08-06 14:18:53');
/*!40000 ALTER TABLE `purchases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchases_products`
--

DROP TABLE IF EXISTS `purchases_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchases_products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `product_price` float DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchases_products`
--

LOCK TABLES `purchases_products` WRITE;
/*!40000 ALTER TABLE `purchases_products` DISABLE KEYS */;
INSERT INTO `purchases_products` VALUES (20,20,1,1,499,'2020-08-06 14:18:49',NULL);
/*!40000 ALTER TABLE `purchases_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchases_transations`
--

DROP TABLE IF EXISTS `purchases_transations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchases_transations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `transaction_code` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchases_transations`
--

LOCK TABLES `purchases_transations` WRITE;
/*!40000 ALTER TABLE `purchases_transations` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchases_transations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  FULLTEXT KEY `name_email_FULLTEXT` (`name`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Rodinei','rodinei@teste.com','123456','2020-07-30 17:01:09',NULL),(2,'Comprador teste','testemercadopago@teste.com','$2y$10$xAddcP9cQ7Y6QCZK/G/HP.dJYb00lwfF.kJDpHrbZLQO/J/VVbNFK','2020-08-06 13:56:35',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-07 13:30:58
