$(function() {
    if (typeof maxValueSlider !== "undefined") {
        slider();
    }
    submitButtons();
    setImage();
});

function slider() {
    var sliderRange = $( "#slider-range" );
    sliderRange.slider({
        range: true,
        min: 0,
        max: maxValueSlider,
        values: [$("#slider0").val() || 0, $("#slider1").val() || 0],
        slide: function( event, ui ) {
            $( "#amount" ).val( "R$" + ui.values[ 0 ] + " - R$" + ui.values[ 1 ] );
        },
        change: function (event, ui) {
            $("#slider" + ui.handleIndex).val(ui.value);
            $(".filterarea form").submit();
        }
    });
    $( "#amount" ).val( "R$" + sliderRange.slider( "values", 0 ) + " - R$" + sliderRange.slider( "values", 1 ) );

    $(".filterarea").find("input").on("change", function (e) {
        $(".filterarea form").submit();
    });
}

function submitButtons() {
    $(".form-add-to-cart button").on("click", function (e) {
        e.preventDefault();
        var inputQty = $(".add-to-cart-qty");
        var qty = parseInt(inputQty.val());
        var action = $(this).attr("data-action");
        if (action === "dec") {
            if (qty - 1 >= 1) {
                qty = qty - 1;
            }
        }
        if (action === "inc") {
            qty = qty + 1;
        }
        inputQty.val(qty);
        $("input[name=product_qty]").val(qty);
    });
}

function setImage() {
    $(".photo-item").on("click", function (e) {
        var url = $(this).find("img").attr("src");
        $(".main-photo").find("img").attr("src", url);
    });
}