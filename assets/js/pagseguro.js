$(function () {
    $("#btn-payment").on("click", function (e) {
        e.preventDefault();

        // Hash pagseguro
        var id = PagSeguroDirectPayment.getSenderHash();

        // comprador
        var name = $("#name").val();
        var cpf = $("#cpf").val();
        var email = $("#email").val();
        var password_rdn = $("#password_rdn").val();
        var dddPhone = $("#dddPhone").val();
        var phone = $("#phone").val();

        // endereço comprador
        var cep = $("#cep").val();
        var address = $("#address").val();
        var number = $("#number").val();
        var complement = $("#complement").val();
        var district = $("#district").val();
        var city = $("#city").val();
        var state = $("#state").val();

        // card
        var cardOwner = $("#cardOwner").val();
        var cardCpfOwner = $("#cardCpfOwner").val();
        var cardNumber = $("#cardNumber").val();
        var cardCVV = $("#cardCVV").val();
        var cardMonth = $("#cardMonth").val();
        var cardYear = $("#cardYear").val();
        var cardInstallment = $("#cardInstallment").val();

        if (cardNumber !== "" && cardCVV !== "" && cardMonth !== "" && cardYear !== "") {
            PagSeguroDirectPayment.createCardToken({
                cardNumber: cardNumber,
                brand: window.brandName,
                cvv: cardCVV,
                expirationMonth: cardMonth,
                expirationYear: cardYear,
                success: function (response) {
                    window.cardToken = response.card.token;
                    $.ajax({
                        url: baseUrl + "ajax/pagseguro_checkout",
                        type: "POST",
                        dataType: "json",
                        data: {
                            id: id,
                            name: name,
                            cpf: cpf,
                            email: email,
                            password_rdn: password_rdn,
                            cep: cep,
                            address: address,
                            number: number,
                            complement: complement,
                            district: district,
                            city: city,
                            state: state,
                            cardOwner: cardOwner,
                            cardCpfOwner: cardCpfOwner,
                            cardNumber: cardNumber,
                            cardCVV: cardCVV,
                            cardMonth: cardMonth,
                            cardYear: cardYear,
                            cardToken: window.cardToken,
                            cardInstallment: cardInstallment,
                            dddPhone: dddPhone,
                            phone: phone
                        },
                        success: function(response) {
                            if (response.error) {
                                alert(response.message);
                            } else {
                                window.location.href = baseUrl + "pagseguro/success"
                            }
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                },
                error: function (error) {
                    console.log(error);
                },
                complete: function (complete) {
                    console.log(complete);
                }
            });
        } else {
            alert("Preencha todos os campos obrigatórios.");
        }
    });

    $("input[name=cardNumber]").on("keyup", function (e) {
       if ($(this).val().length === 6) {
           var total = $("#total").val();
           PagSeguroDirectPayment.getBrand({
               cardBin: $(this).val(),
               success: function(responseBrand) {
                   window.brandName = responseBrand.brand.name;
                   $("input[name=cardCVV]").attr("maxlength", responseBrand.brand.cvvSize);
                   PagSeguroDirectPayment.getInstallments({
                       amount: total,
                       brand: window.brandName,
                       success: function (responseInstallment) {
                           if (responseInstallment.error === false) {
                               var installment = responseInstallment.installments[window.brandName];
                               $("#cardInstallment option").remove();
                               installment.forEach(item => {
                                   var optionValue = item.quantity+';'+item.installmentAmount+';'+item.interestFree;
                                   $("#cardInstallment")
                                       .append('<option value="'+optionValue+'">'+ item.quantity + 'x de R$ ' +item.installmentAmount+'</option>');
                               })
                           }
                       },
                       error: function (errorInstallment) {
                           console.log("getInstallments", errorInstallment);
                       },
                       complete: function (completeInstallment) {
                           console.log("getInstallments",completeInstallment);
                       }
                   });
               },
               error: function (errorBrand) {
                   console.log("getBrand", errorBrand);
               },
               complete: function (completeReponse) {
                   console.log("getBrand", completeReponse);
               }
           });
       }
   }) ;
});