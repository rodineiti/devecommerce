$(function () {
    if ($("#content").length) {
        tinymce.init({
            selector:"#content",
            height:500,
            menubar:false,
            plugins:["textcolor image media autolink lists checklist"],
            toolbar:"undo redo | formatselect | bold italic backcolor | media image | alignleft aligncenter alignright alignjustify | bullist numlist | removeformat",
            automatic_uploads:true,
            file_picker_types:"image",
            images_upload_url:baseUrl+"admin/pages/upload"
        });
    }
    if ($("#description").length) {
        tinymce.init({
            selector:"#description",
            height:300,
            menubar:false,
            plugins:["textcolor image media autolink lists checklist"],
            toolbar:"undo redo | formatselect | bold italic backcolor | media image | alignleft aligncenter alignright alignjustify | bullist numlist | removeformat",
        });
    }

    $("#btn-add-input").on("click", function (e) {
        e.preventDefault();
        $(".area-images").append('<input type="file" name="images[]" class="form-control" />');
    });
});