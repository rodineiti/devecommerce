<div class="row">
    <div class="col">
        <div class="container" style="width: 600px;">
            <h1 class="text-center">Checkout Transparente - PagSeguro<?=$sessionCode?></h1>
            <form action="">
                <h3>Dados pessoais</h3>
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" name="name" id="name" value="Comprador teste" required />
                </div>
                <div class="form-group">
                    <label for="cpf">CPF:</label>
                    <input type="text" class="form-control" name="cpf" id="cpf" value="12345678909" required />
                </div>
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="email" class="form-control" name="email" id="email" value="c63452461647404685529@sandbox.pagseguro.com.br" required />
                </div>
                <div class="form-group">
                    <label for="password_rdn">Senha</label>
                    <input type="password" class="form-control" name="password_rdn" id="password_rdn" value="n6653d8b825f173k" required />
                </div>
                <h3>Informações de Endereço</h3>
                <div class="form-group">
                    <label for="cep">CEP</label>
                    <input type="text" class="form-control" name="cep" id="cep" value="05338100" required />
                </div>
                <div class="form-group">
                    <label for="address">Logradouro</label>
                    <input type="text" class="form-control" name="address" id="address" value="Rua eulo maroni" required />
                </div>
                <div class="form-group">
                    <label for="number">Número</label>
                    <input type="text" class="form-control" name="number" id="number" value="101" required />
                </div>
                <div class="form-group">
                    <label for="complement">Complemento</label>
                    <input type="text" class="form-control" name="complement" id="complement" value="ap" />
                </div>
                <div class="form-group">
                    <label for="district">Bairro</label>
                    <input type="text" class="form-control" name="district" id="district" value="jaguaré" required />
                </div>
                <div class="form-group">
                    <label for="city">Cidade</label>
                    <input type="text" class="form-control" name="city" id="city" value="são paulo" required />
                </div>
                <div class="form-group">
                    <label for="state">Estado</label>
                    <input type="text" class="form-control" name="state" id="state" value="SP" required />
                </div>
                <div class="form-group">
                    <label for="dddPhone">DDD (Telefone)</label>
                    <input type="number" maxlength="2" class="form-control" name="dddPhone" id="dddPhone" value="11" required />
                </div>
                <div class="form-group">
                    <label for="phone">Telefone</label>
                    <input type="number" maxlength="8" class="form-control" name="phone" id="phone" value="41423080" required />
                </div>
                <h3>Informações de Pagamento</h3>
                <div class="form-group">
                    <label for="cardOwner">Titular do Cartão</label>
                    <input type="text" class="form-control" name="cardOwner" id="cardOwner" value="Comprador teste" />
                </div>
                <div class="form-group">
                    <label for="cardCpfOwner">CPF do Titular do Cartão</label>
                    <input type="text" class="form-control" name="cardCpfOwner" id="cardCpfOwner" value="12345678909" />
                </div>
                <div class="form-group">
                    <label for="cardNumber">Número do Cartão</label>
                    <input type="text" class="form-control" name="cardNumber" id="cardNumber" value="" required />
                </div>
                <div class="form-group">
                    <label for="cardCVV">Código de Segurança</label>
                    <input type="text" class="form-control" name="cardCVV" id="cardCVV" value="123" required />
                </div>
                <div class="form-group">
                    <label for="cardMonth">Mês vencimento</label>
                    <select class="form-control" name="cardMonth" id="cardMonth" required>
                        <?php for ($i = 1; $i <= 12; $i++): ?>
                        <option <?=selected($i === 12)?> value="<?=$i < 10 ? "0".$i : $i?>"><?=$i < 10 ? "0".$i : $i?></option>
                        <?php endfor; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="cardYear">Ano vencimento</label>
                    <select class="form-control" name="cardYear" id="cardYear" required>
                        <?php for ($i = intval(date("Y")); $i <= intval(date("Y")) + 20; $i++): ?>
                            <option <?=selected($i === 2030)?> value="<?=$i?>"><?=$i?></option>
                        <?php endfor; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="cardInstallment">Parcelas</label>
                    <select class="form-control" name="cardInstallment" id="cardInstallment" required></select>
                </div>
                <input type="hidden" name="total" id="total" value="<?=$total?>" />
                <input type="submit" class="btn btn-success btn-custom-success" id="btn-payment" value="Efetuar Compra" />
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>
<script type="text/javascript" src="<?=asset("js/pagseguro.js")?>"></script>
<script type="text/javascript">
    PagSeguroDirectPayment.setSessionId('<?=$sessionCode?>');
</script>