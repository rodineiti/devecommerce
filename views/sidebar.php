<aside>
    <h1><?php $this->lang->get("FILTER")?></h1>
    <div class="filterarea">
        <form method="get">
            <input type="hidden" name="s" value="<?=(isset($data["searchTerm"])) ? $data["searchTerm"] : ""?>">
            <input type="hidden" name="category" value="<?=(isset($data["searchCategory"])) ? $data["searchCategory"] : ""?>">
            <div class="filter-box">
                <h4><?php $this->lang->get("BRANDS")?></h4>
                <div class="filter-content">
                    <?php foreach ($data["filters"]["brands"] as $item): ?>
                        <div class="filter-item">
                            <input type="checkbox"
                                <?=checked(isset($data["filter_selected"]["brand"]) && in_array($item->id, $data["filter_selected"]["brand"]))?>
                                   name="filter[brand][]" value="<?=$item->id?>" id="filter_brand<?=$item->id?>" />
                            <label for="filter_brand<?=$item->id?>"><?=$item->name?></label><span style="float: right;">(<?=$item->total?>)</span>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="filter-box">
                <h4><?php $this->lang->get("PRICE")?></h4>
                <div class="filter-content">
                    <div class="filter-item">
                        <input type="hidden" id="slider0" name="filter[slider0]" value="<?=$data["filters"]["slider0"]?>" />
                        <input type="hidden" id="slider1" name="filter[slider1]" value="<?=$data["filters"]["slider1"]?>" />
                        <input type="text" id="amount" readonly style="border:0;">
                        <div id="slider-range"></div>
                    </div>
                </div>
            </div>
            <div class="filter-box">
                <h4><?php $this->lang->get("RATING")?></h4>
                <div class="filter-content">
                    <div class="filter-item">
                        <input type="checkbox" name="filter[star][]" value="0" id="filter_star0"
                            <?=checked(isset($data["filter_selected"]["star"]) && in_array("0", $data["filter_selected"]["star"]))?> />
                        <label for="filter_star0">
                            (<?php $this->lang->get("NOSTAR")?>)
                        </label>
                        <span style="float: right;">(<?=$data["filters"]["stars"]["0"]?>)</span>
                    </div>
                    <div class="filter-item">
                        <input type="checkbox" name="filter[star][]" value="1" id="filter_star1"
                            <?=checked(isset($data["filter_selected"]["star"]) && in_array("1", $data["filter_selected"]["star"]))?> />
                        <label for="filter_star1">
                            <img src="<?=image("star.png")?>" alt="star" height="20">
                        </label>
                        <span style="float: right;">(<?=$data["filters"]["stars"]["1"]?>)</span>
                    </div>
                    <div class="filter-item">
                        <input type="checkbox" name="filter[star][]" value="2" id="filter_star2"
                            <?=checked(isset($data["filter_selected"]["star"]) && in_array("2", $data["filter_selected"]["star"]))?> />
                        <label for="filter_star2">
                            <img src="<?=image("star.png")?>" alt="star" height="20">
                            <img src="<?=image("star.png")?>" alt="star" height="20">
                        </label>
                        <span style="float: right;">(<?=$data["filters"]["stars"]["2"]?>)</span>
                    </div>
                    <div class="filter-item">
                        <input type="checkbox" name="filter[star][]" value="3" id="filter_star3"
                            <?=checked(isset($data["filter_selected"]["star"]) && in_array("3", $data["filter_selected"]["star"]))?> />
                        <label for="filter_star3">
                            <img src="<?=image("star.png")?>" alt="star" height="20">
                            <img src="<?=image("star.png")?>" alt="star" height="20">
                            <img src="<?=image("star.png")?>" alt="star" height="20">
                        </label>
                        <span style="float: right;">(<?=$data["filters"]["stars"]["3"]?>)</span>
                    </div>
                    <div class="filter-item">
                        <input type="checkbox" name="filter[star][]" value="4" id="filter_star4"
                            <?=checked(isset($data["filter_selected"]["star"]) && in_array("4", $data["filter_selected"]["star"]))?> />
                        <label for="filter_star4">
                            <img src="<?=image("star.png")?>" alt="star" height="20">
                            <img src="<?=image("star.png")?>" alt="star" height="20">
                            <img src="<?=image("star.png")?>" alt="star" height="20">
                            <img src="<?=image("star.png")?>" alt="star" height="20">
                        </label>
                        <span style="float: right;">(<?=$data["filters"]["stars"]["4"]?>)</span>
                    </div>
                    <div class="filter-item">
                        <input type="checkbox" name="filter[star][]" value="5" id="filter_star5"
                            <?=checked(isset($data["filter_selected"]["star"]) && in_array("5", $data["filter_selected"]["star"]))?> />
                        <label for="filter_star5">
                            <img src="<?=image("star.png")?>" alt="star" height="20">
                            <img src="<?=image("star.png")?>" alt="star" height="20">
                            <img src="<?=image("star.png")?>" alt="star" height="20">
                            <img src="<?=image("star.png")?>" alt="star" height="20">
                            <img src="<?=image("star.png")?>" alt="star" height="20">
                        </label>
                        <span style="float: right;">(<?=$data["filters"]["stars"]["5"]?>)</span>
                    </div>
                </div>
            </div>
            <div class="filter-box">
                <h4><?php $this->lang->get("SALE")?></h4>
                <div class="filter-content">
                    <div class="filter-item">
                        <input type="checkbox" name="filter[sale]" value="1" id="filter_sale"
                            <?=checked(isset($data["filter_selected"]["sale"]) && $data["filter_selected"]["sale"] == "1")?> />
                        <label for="filter_sale"><?php $this->lang->get("INPROMOTION")?></label>
                        <span style="float: right;">(<?=$data["filters"]["sale"]?>)</span>
                    </div>
                </div>
            </div>
            <div class="filter-box">
                <h4><?php $this->lang->get("OPTIONS")?></h4>
                <div class="filter-content">
                    <?php foreach ($data["filters"]["options"] as $item): ?>
                        <strong><?=$item["name"]?></strong><br>
                        <?php foreach ($item["options"] as $option): ?>
                            <div class="filter-item">
                                <input type="checkbox" name="filter[options][]"
                                       value="<?=$option["description"]?>" id="filter_option<?=$option["option_id"]?>"
                                    <?=checked(isset($data["filter_selected"]["options"]) && in_array($option["description"], $data["filter_selected"]["options"]))?> />
                                <label for="filter_option<?=$option["option_id"]?>"><?=$option["description"]?></label>
                                <span style="float: right;">(<?=$option["total"]?>)</span>
                            </div>
                        <?php endforeach; ?>
                        <br>
                    <?php endforeach; ?>
                </div>
            </div>
        </form>
    </div>

    <div class="widget">
        <h1><?php $this->lang->get("FEATURESPRODUCTS")?></h1>
        <div class="widget_body">
            <?=$this->view("widget_item", ["list" => $data["widget_feature_1"]])?>
        </div>
    </div>
</aside>