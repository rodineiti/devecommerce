<?php foreach ($items as $item): ?>
    <tr>
        <th scope="row"><?= $item["id"] ?></th>
        <td><?php for ($i = 0; $i < $level; $i++) echo "-- " ?><?= $item["name"] ?></td>
        <td><?= $item["created_at"] ?></td>
        <td>
            <?php if (hasPermission("{$prefix}-edit")): ?>
                <a href="<?= BASE_URL . "admin/{$redirect}/edit/" . $item["id"]; ?>" class="btn btn-info">Editar</a>
            <?php endif; ?>
            <?php if (hasPermission("{$prefix}-destroy")): ?>
                <a href="<?= BASE_URL . "admin/{$redirect}/destroy/" . $item["id"]; ?>" class="btn btn-danger">Deletar</a>
            <?php endif; ?>
        </td>
    </tr>

    <?php if (isset($item["subs"]) && count($item["subs"])): ?>
        <?=$this->view("admin_category_item", [
            "items" => $item["subs"],
            "level" => $level + 1,
            "prefix" => $prefix,
            "redirect" => $redirect
        ])?>
    <?php endif; ?>
<?php endforeach; ?>