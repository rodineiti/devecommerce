<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=SITE_NAME?></title>
    <link href="<?= asset("css/admin/bootstrap.min.css") ?>" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="<?= asset("css/admin/style_admin.css") ?>">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light mb-2">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="<?= BASE_URL . "admin/home" ?>" class="navbar-brand"><?=SITE_NAME?></a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <?php if(auth("admins")): ?>
                <li class="dropdown mr-2">
                    <a class="nav-item nav-link dropdown-toggle" data-toggle="dropdown" href="javascript:;"><?=auth("admins")->name?></a>
                    <ul class="dropdown-menu">
                        <li><a class="nav-item nav-link" href="<?= BASE_URL ?>admin/profile"><?=auth("admins")->name?></a></li>
                        <li><a class="nav-item nav-link" href="<?= BASE_URL ?>admin/logout">Sair</a></li>
                    </ul>
                </li>
            <?php endif; ?>
        </ul>
    </div>
</nav>

<?=$this->viewTemplate($view, $data)?>
<div class="mb-5"></div>
<script src="<?=asset("js/admin/jquery.min.js")?>"></script>
<script src="<?=asset("js/admin/bootstrap.min.js")?>"></script>
<script src="https://cdn.tiny.cloud/1/fvixi3kmpjgenyrb4d09yzdqvtcuc47yydhu8wyiymmgsciv/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>var baseUrl = '<?=BASE_URL?>';</script>
<script src="<?=asset("js/admin/script_admin.js")?>"></script>
</body>
</html>