<div class="row">
    <div class="col">
        <div class="container" style="width: 600px;">
            <h1 class="text-center">Checkout Boleto</h1>
            <form action="<?= BASE_URL . "boleto/payment"?>" method="post">
                <h3>Dados pessoais</h3>
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" name="name" id="name" value="Comprador teste" required />
                </div>
                <div class="form-group">
                    <label for="cpf">CPF:</label>
                    <input type="text" class="form-control" name="cpf" id="cpf" value="12345678909" required />
                </div>
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="email" class="form-control" name="email" id="email" value="testemercadopago@teste.com" required />
                </div>
                <div class="form-group">
                    <label for="password_rdn">Senha</label>
                    <input type="password" class="form-control" name="password_rdn" id="password_rdn" value="n6653d8b825f173k" required />
                </div>
                <h3>Informações de Endereço</h3>
                <div class="form-group">
                    <label for="cep">CEP</label>
                    <input type="text" class="form-control" name="cep" id="cep" value="05338100" required />
                </div>
                <div class="form-group">
                    <label for="address">Logradouro</label>
                    <input type="text" class="form-control" name="address" id="address" value="Rua eulo maroni" required />
                </div>
                <div class="form-group">
                    <label for="number">Número</label>
                    <input type="text" class="form-control" name="number" id="number" value="101" required />
                </div>
                <div class="form-group">
                    <label for="complement">Complemento</label>
                    <input type="text" class="form-control" name="complement" id="complement" value="ap" />
                </div>
                <div class="form-group">
                    <label for="district">Bairro</label>
                    <input type="text" class="form-control" name="district" id="district" value="jaguaré" required />
                </div>
                <div class="form-group">
                    <label for="city">Cidade</label>
                    <input type="text" class="form-control" name="city" id="city" value="são paulo" required />
                </div>
                <div class="form-group">
                    <label for="state">Estado</label>
                    <input type="text" class="form-control" name="state" id="state" value="SP" required />
                </div>
                <div class="form-group">
                    <label for="dddPhone">DDD (Telefone)</label>
                    <input type="number" maxlength="2" class="form-control" name="dddPhone" id="dddPhone" value="11" required />
                </div>
                <div class="form-group">
                    <label for="phone">Telefone</label>
                    <input type="number" maxlength="8" class="form-control" name="phone" id="phone" value="41423080" required />
                </div>
                <input type="submit" class="btn btn-success btn-custom-success" value="Efetuar Compra" />
            </form>
        </div>
    </div>
</div>
