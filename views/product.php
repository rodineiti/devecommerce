<div class="row">
    <div class="col-sm-5">
        <div class="main-photo">
            <img src="<?=$product->images[0]->url?>" alt="product" class="img-responsive" />
        </div>
        <div class="photo-gallery">
            <div class="row" style="margin-top:5px;">
                <?php foreach ($product->images as $image): ?>
                    <div class="col-sm-3 photo-item">
                        <img src="<?=$image->url?>" alt="product-image" class="img-responsive" />
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="col-sm-7">
        <h2><?=$product->name?></h2>
        <small><?=$product->brand?></small><br>
        <?php if ($product->rating != "0"): ?>
            <?php for ($i = 0; $i < intval($product->rating); $i++): ?>
                <img src="<?=image("star.png")?>" alt="star" height="20" />
            <?php endfor; ?>
        <?php endif; ?>
        <hr>
        <p><?=$product->description?></p>
        <hr>
        De: <span class="price-from"><?=str_price($product->price_from)?></span><br>
        Por: <span class="original-price"><?=str_price($product->price)?></span>

        <form action="<?= BASE_URL . "cart/add/" . $product->id ?>" method="post" class="form-add-to-cart">
            <button class="btn-dec" data-action="dec">-</button>
            <input type="text" class="add-to-cart-qty" name="qty" value="1" disabled />
            <input type="hidden" name="product_qty" value="1" />
            <button class="btn-inc" data-action="inc">+</button>
            <input type="submit" value="<?php $this->lang->get("ADDCART") ?>" />
        </form>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-sm-6">
        <h3><?php $this->lang->get("PRODUCT_SPECIFICATION") ?></h3>
        <?php foreach ($product->productOptions as $option): ?>
            <strong><?=$option["name"]?></strong>:
            <?php foreach ($option["options"] ?? [] as $op): ?>
                <p><?=$op["description"]?></p>
            <?php endforeach; ?>
            <br>
        <?php endforeach; ?>
    </div>
    <div class="col-sm-6">
        <h3><?php $this->lang->get("PRODUCT_REVIEW") ?></h3>
        <?php foreach ($product->productRates as $rate): ?>
            <?php if ($rate->points != "0"): ?>
                <?php for ($i = 0; $i < intval($rate->points); $i++): ?>
                    <img src="<?=image("star.png")?>" alt="star" height="20" />
                <?php endfor; ?>
            <?php endif; ?>
            <p><strong><?=$rate->user->name?></strong></p>
            <p>"<?=$rate->description?>"</p>
            <hr>
        <?php endforeach; ?>
    </div>
</div>