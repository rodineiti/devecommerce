<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=SITE_NAME?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet" type="text/css">
    <link href="<?=asset("css/bootstrap.min.css")?>" rel="stylesheet" id="bootstrap-css">
    <link href="<?=asset("css/jquery-ui.min.css") ?>" rel="stylesheet" id="jquery-ui-css">
    <link rel="stylesheet" href="<?=asset("css/style.css")?>">

    <script src="<?=asset("js/jquery.min.js")?>"></script>
</head>
<body>

<nav class="navbar topnav">
    <div class="container">
        <ul class="nav navbar-nav">
            <li class="active"><a href="<?php echo BASE_URL; ?>"><?php $this->lang->get("HOME")?></a></li>
            <li><a href="<?php echo BASE_URL; ?>contact"><?php $this->lang->get("CONTACT")?></a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php $this->lang->get("PORTUGUESE")?>
                    <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="<?= BASE_URL . "lang/index/pt-br"; ?>"><?php $this->lang->get("PORTUGUESE")?></a></li>
                    <li><a href="<?= BASE_URL . "lang/index/en"; ?>"><?php $this->lang->get("ENGLISH")?></a></li>
                    <li><a href="<?= BASE_URL . "lang/index/es"; ?>"><?php $this->lang->get("SPANISH")?></a></li>
                </ul>
            </li>
            <?php if(auth()): ?>
                <li class="dropdown">
                    <a class="nav-item nav-link dropdown-toggle" data-toggle="dropdown" href="javascript:;"><?=auth()->name?></a>
                    <ul class="dropdown-menu">
                        <li><a class="nav-item nav-link" href="<?= BASE_URL ?>auth/profile"><?=auth()->name?></a></li>
                        <li><a class="nav-item nav-link" href="<?= BASE_URL ?>auth/logout">Sair</a></li>
                    </ul>
                </li>
            <?php else: ?>
                <li><a href="<?php echo BASE_URL; ?>auth?login"><?php $this->lang->get("LOGIN")?></a></li>
                <li><a href="<?php echo BASE_URL; ?>auth/register"><?php $this->lang->get("REGISTER")?></a></li>
            <?php endif; ?>
        </ul>
    </div>
</nav>
<header>
    <div class="container">
        <div class="row">
            <div class="col-sm-2 logo">
                <a href="<?php echo BASE_URL; ?>"><img src="<?= image("logo.png"); ?>" alt="image" /></a>
            </div>
            <div class="col-sm-7">
                <div class="head_help"><?=CONF_CONTACT_PHONE?></div>
                <div class="head_email"><?=CONF_CONTACT_EMAIL?></div>

                <div class="search_area">
                    <form action="<?= BASE_URL . "search"?>" method="GET">
                        <input type="text" name="s" id="s" required
                               value="<?=(isset($data["searchTerm"])) ? $data["searchTerm"] : ""?>"
                               placeholder="<?php $this->lang->get("SEARCHITEM")?>" />
                        <select name="category" id="category">
                            <option value=""><?php $this->lang->get("ALLCATEGORIES")?></option>
                            <?php foreach ($data["categories"] as $category): ?>
                                <option
                                    <?=selected(isset($data["searchCategory"]) && $data["searchCategory"] == $category["id"])?>
                                        value="<?=$category["id"]?>"><?=$category["name"]?></option>
                                <?php if (count($category["subs"])): ?>
                                    <?=$this->view("subcategory", [
                                        "type" => "options",
                                        "subs" => $category["subs"],
                                        "level" => 1,
                                        "category" => isset($data["searchCategory"]) ? $data["searchCategory"] : null
                                    ]);?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </select>
                        <input type="submit" value="" />
                    </form>
                </div>
            </div>
            <div class="col-sm-3">
                <a href="<?php echo BASE_URL; ?>cart">
                    <div class="cartarea">
                        <div class="carticon">
                            <div class="cartqt"><?=$data["cart_qty"]?></div>
                        </div>
                        <div class="carttotal">
                            <?php $this->lang->get("YOUCART")?>:<br/>
                            <span><?=str_price($data["cart_subtotal"])?></span>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</header>
<div class="categoryarea">
    <nav class="navbar">
        <div class="container">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php $this->lang->get("SELECTCATEGORY")?>
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <?php foreach ($data["categories"] as $category): ?>
                            <li><a href="<?= BASE_URL . "categories/show/".$category["id"] ?>"><?=$category["name"]?></a></li>
                            <?php if (count($category["subs"])): ?>
                                <?=$this->view("subcategory", ["type" => "lis", "subs" => $category["subs"], "level" => 1])?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </li>
                <?php if (isset($data["categories_filter"])): ?>
                    <?php foreach ($data["categories_filter"] as $category): ?>
                        <li><a href="<?= BASE_URL . "categories/show/" . $category->id ?>"><?=$category->name?></a></li>
                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>
        </div>
    </nav>
</div>
<section>
    <div class="container">
        <div class="row">
            <?php if (isset($data["sidebar"])): ?>
                <div class="col-sm-3">
                    <?=$this->view("sidebar", ["data" => $data])?>
                </div>
                <div class="col-sm-9"><?php $this->viewTemplate($view, $data); ?></div>
            <?php else: ?>
                <div class="col-sm-12"><?php $this->viewTemplate($view, $data); ?></div>
            <?php endif; ?>
        </div>
    </div>
</section>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="widget">
                    <h1><?php $this->lang->get("FEATURESPRODUCTS")?></h1>
                    <div class="widget_body">
                        <?=$this->view("widget_item", ["list" => $data["widget_feature_2"]])?>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="widget">
                    <h1><?php $this->lang->get("SALEPRODUCTS")?></h1>
                    <div class="widget_body">
                        <?=$this->view("widget_item", ["list" => $data["widget_sale"]])?>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="widget">
                    <h1><?php $this->lang->get("TOPPRODUCTS")?></h1>
                    <div class="widget_body">
                        <?=$this->view("widget_item", ["list" => $data["widget_toprated"]])?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="subarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2 no-padding">
                    <form action="https://rdndeveloper.us16.list-manage.com/subscribe/post?u=30385257f15c6bab49e2acb4d&amp;id=7db5c44307"
                          method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                        <input type="email" value="" name="EMAIL" class="subemail" id="mce-EMAIL" placeholder="<?php $this->lang->get("SUBSCRIBENEWS")?>" required>
                        <input type="hidden" name="b_30385257f15c6bab49e2acb4d_7db5c44307" tabindex="-1" value="">
                        <input type="submit" value="<?php $this->lang->get("SUBSCRIBENEWSBUTTON")?>" name="subscribe" id="mc-embedded-subscribe" class="button">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="links">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <a href="<?php echo BASE_URL; ?>"><img width="150" src="<?= image("logo.png"); ?>" alt="image" /></a><br/><br/>
                    <strong><?=SITE_NAME?></strong><br/><br/>
                    <?=CONF_CONTACT_ADDRESS?>
                </div>
                <div class="col-sm-8 linkgroups">
                    <div class="row">
                        <div class="col-sm-4">
                            <h3><?php $this->lang->get("CATEGORIES")?></h3>
                            <ul>
                                <li><a href="#">Categoria X</a></li>
                                <li><a href="#">Categoria X</a></li>
                                <li><a href="#">Categoria X</a></li>
                                <li><a href="#">Categoria X</a></li>
                                <li><a href="#">Categoria X</a></li>
                                <li><a href="#">Categoria X</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-4">
                            <h3><?php $this->lang->get("INFORMATION")?></h3>
                            <ul>
                                <li><a href="#">Menu 1</a></li>
                                <li><a href="#">Menu 2</a></li>
                                <li><a href="#">Menu 3</a></li>
                                <li><a href="#">Menu 4</a></li>
                                <li><a href="#">Menu 5</a></li>
                                <li><a href="#">Menu 6</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-4">
                            <h3><?php $this->lang->get("INFORMATION")?></h3>
                            <ul>
                                <li><a href="#">Menu 1</a></li>
                                <li><a href="#">Menu 2</a></li>
                                <li><a href="#">Menu 3</a></li>
                                <li><a href="#">Menu 4</a></li>
                                <li><a href="#">Menu 5</a></li>
                                <li><a href="#">Menu 6</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">© <span>Loja 2.0</span> - <?php $this->lang->get("COPYRIGHT") ?></div>
                <div class="col-sm-6">
                    <div class="payments">
                        <img src="<?= image("visa.png") ?>" alt="image" />
                        <img src="<?= image("visa.png") ?>" alt="image" />
                        <img src="<?= image("visa.png") ?>" alt="image" />
                        <img src="<?= image("visa.png") ?>" alt="image" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<script>
    var baseUrl = '<?=BASE_URL?>';
    var maxValueSlider = <?=isset($data["filters"]) && $data["filters"]["maxValueSlider"] ? $data["filters"]["maxValueSlider"] : 0;?>;
</script>
<script src="<?=asset("js/bootstrap.min.js")?>"></script>
<script src="<?=asset("js/jquery-ui.min.js")?>"></script>
<script src="<?=asset("js/script.js")?>"></script>
</body>
</html>