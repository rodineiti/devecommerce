<?php foreach ($list as $item): ?>
    <div class="widget-item">
        <a href="<?= BASE_URL . "products/show/" . $item->id; ?>">
            <div class="widget-detail">
                <div class="widget-name"><?=$item->name?></div>
                <div class="widget-price"><span>R$ <?=str_price($item->price_from)?></span> R$ <?=str_price($item->price)?></div>
            </div>
            <div class="widget-image">
                <img src="<?=$item->images[0]->url?>" alt="image">
            </div>
        </a>
    </div>
<?php endforeach; ?>