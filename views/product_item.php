<?php if (isset($product)): ?>
    <div class="product-item">
        <a href="<?= BASE_URL . "products/show/" . $product->id; ?>">
            <div class="product-tags">
                <?php if ($product->sale): ?>
                <div class="product-tag product-tag-red"><?php $this->lang->get("SALE")?></div>
                <?php endif; ?>
                <?php if ($product->bestseller): ?>
                <div class="product-tag product-tag-green"><?php $this->lang->get("BESTSELLER")?></div>
                <?php endif; ?>
                <?php if ($product->new_add): ?>
                <div class="product-tag product-tag-blue"><?php $this->lang->get("NEW")?></div>
                <?php endif; ?>
            </div>
            <div class="product-image">
                <?php if (isset($product->images) && count($product->images)): ?>
                <img src="<?=$product->images[0]->url?>" alt="image" width="100%">
                <?php endif; ?>
            </div>
            <div class="product-name"><?=$product->name?></div>
            <div class="product-brand"><?=$product->brand?></div>
            <div class="product-price-from"><?=$product->price_from && $product->price_from > 0 ? "R$ " . str_price($product->price_from) : "" ?></div>
            <div class="product-price"><?=$product->price ? "R$ " . str_price($product->price) : "" ?></div>
            <div style="clear: both;"></div>
        </a>
    </div>
<?php endif; ?>
