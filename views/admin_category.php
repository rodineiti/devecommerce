<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin_menu");?>
        </div>
        <div class="col-md-9">
            <h1 class="text-center">Lista de Categorias</h1>
            <hr>
            <?php if (hasPermission("{$prefix}-create")): ?>
                <a href="<?= BASE_URL . "admin/{$redirect}/create"; ?>" class="btn btn-primary mb-2">Adicionar</a>
            <?php endif; ?>
            <?php if (isset($_GET["error"])): ?>
                <div class="alert alert-danger">
                    Opss. Ocorreu um erro no processamento, tente mais tarde.
                </div>
            <?php endif; ?>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Criado em</th>
                    <th scope="col">Opções</th>
                </tr>
                </thead>
                <tbody>
                <?=$this->view("admin_category_item", [
                    "items" => $list,
                    "level" => 0,
                    "prefix" => $prefix,
                    "redirect" => $redirect
                ])?>
                </tbody>
            </table>
        </div>
    </div>
</div>