<?php if (isset($type) && strtolower($type) === "lis"): ?>
    <?php foreach ($subs as $sub): ?>
        <li>
            <a href="<?= BASE_URL . "categories/show/".$sub["id"] ?>">
                <?php for ($i = 0; $i < $level; $i++) echo "-- " ?>
                <?=$sub["name"]?>
            </a>
        </li>
        <?php if (count($sub["subs"])): ?>
            <?=$this->view("subcategory", ["type" => strtolower($type), "subs" => $sub["subs"], "level" => $level + 1])?>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>

<?php if (isset($type) && strtolower($type) === "options"): ?>
    <?php foreach ($subs as $sub): ?>
        <option value="<?=$sub["id"]?>"
            <?=selected(isset($category) && $category == $sub["id"])?>>
            <?php for ($i = 0; $i < $level; $i++) echo "-- " ?>
            <?=$sub["name"]?>
        </option>
        <?php if (count($sub["subs"])): ?>
            <?=$this->view("subcategory", [
                "type" => strtolower($type),
                "subs" => $sub["subs"],
                "level" => $level + 1,
                "category" => isset($category) ? $category : null
            ])?>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>