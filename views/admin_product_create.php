<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin_menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= BASE_URL . "admin/{$redirect}/index"; ?>" class="btn btn-info mb-2">Voltar</a>
            <?php if (isset($_GET["error"]) && $_GET["error"] === "fields"): ?>
                <div class="alert alert-warning">
                    Preencha todos os campos!
                </div>
            <?php endif; ?>
            <?php if (isset($_GET["success"])): ?>
                <div class="alert alert-success">
                    <strong>OK!</strong> Criado sucesso.
                </div>
            <?php endif; ?>
            <h1>Adicionar produto</h1>
            <form method="POST" action="<?= BASE_URL?>admin/<?=$redirect?>/store" enctype="multipart/form-data">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="category_id">Categoria:</label>
                            <select name="category_id" id="category_id" class="form-control" required>
                                <option value="">Selecione</option>
                                <?php foreach ($categories as $category): ?>
                                    <option value="<?=$category["id"]?>"><?=$category["name"]?></option>
                                    <?php if (count($category["subs"])): ?>
                                        <?=$this->view("subcategory", [
                                            "type" => "options",
                                            "subs" => $category["subs"],
                                            "level" => 1,
                                            "category" => null
                                        ]);?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="brand_id">Marca:</label>
                            <select name="brand_id" id="brand_id" class="form-control" required>
                                <option value="">Selecione</option>
                                <?php foreach ($brands as $brand): ?>
                                    <option value="<?=$brand->id?>"><?=$brand->name?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name">Nome:</label>
                    <input type="text" name="name" id="name" class="form-control" required />
                </div>
                <div class="form-group">
                    <label for="description">Descrição:</label>
                    <textarea name="description" id="description" class="form-control"></textarea>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="stock">Estoque:</label>
                            <input type="number" name="stock" id="stock" class="form-control" required />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="price_from">Preço (de):</label>
                            <input type="text" name="price_from" id="price_from" class="form-control" required />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="price">Preço (por):</label>
                            <input type="text" name="price" id="price" class="form-control" required />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="weight">Peso (em Kg):</label>
                            <input type="number" name="weight" id="weight" class="form-control" required />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="width">Largura (em Cm):</label>
                            <input type="number" name="width" id="width" class="form-control" required />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="height">Altura (em Cm):</label>
                            <input type="number" name="height" id="height" class="form-control" required />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="length">Comprimeito (em Cm):</label>
                            <input type="number" name="length" id="length" class="form-control" required />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="diameter">Diametro (em cm):</label>
                            <input type="number" name="diameter" id="diameter" class="form-control" required />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="checkbox" name="featured" id="featured" value="1" />
                            <label for="featured">Em destaque:</label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="checkbox" name="sale" id="sale" value="1" />
                            <label for="sale">Em promoção:</label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="checkbox" name="bestseller" id="bestseller" value="1" />
                            <label for="bestseller">Mais vendidos:</label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="checkbox" name="new_add" id="new_add" value="1" />
                            <label for="new_add">Novo produto</label>
                        </div>
                    </div>
                </div>
                <hr>

                <div class="row">
                    <?php foreach ($options as $option): ?>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="option-<?=$option->id?>"><?=$option->name?></label>
                                <input type="text" name="options[<?=$option->id?>]" id="option-<?=$option->id?>" class="form-control" />
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>

                <hr>

                <div class="row">
                    <div class="col">
                        <label for="images-area">Imagens do produto</label><br>
                        <button class="btn btn-primary mt-1 mb-2" id="btn-add-input">+</button>
                        <div class="form-group area-images mt-1">
                            <input type="file" name="images[]" class="form-control" />
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <input type="submit" value="Criar Produto" class="btn btn-primary" />
                </div>
            </form>
        </div>
    </div>
</div>