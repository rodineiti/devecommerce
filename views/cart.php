<h1>Carrinho de compras</h1>
<table class="table">
    <thead>
    <tr>
        <th scope="col"></th>
        <th scope="col">Nome</th>
        <th scope="col">Quantidade</th>
        <th scope="col">Preço</th>
        <th scope="col"></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($products_cart as $key => $item): ?>
        <tr>
            <th scope="row">
                <a href="<?= BASE_URL . "products/show/" . $item["id"] ?>">
                    <img src="<?=$item["image"]?>" class="img-responsive" alt="img" width="80">
                </a>
            </th>
            <td><?=$item["name"]?></td>
            <td>
                <div style="display: flex;">
                    <a href="<?= BASE_URL . "cart/dec/" . $item["id"] ?>" class="btn btn-default">-</a>
                    <input type="text" class="form-control add-to-cart-qty" name="qty" value="<?=$item["qty"]?>" disabled />
                    <a href="<?= BASE_URL . "cart/inc/" . $item["id"] ?>" class="btn btn-default">+</a>
                </div>
            </td>
            <td>R$ <?=str_price($item["price"])?></td>
            <td>
                <a href="<?= BASE_URL . "cart/del/" . $item["id"] ?>"><img src="<?=image("delete.png")?>" alt="delete" height="20" /></a>
            </td>
        </tr>
        <?php endforeach; ?>
        <tr>
            <td align="right" colspan="3">Subtotal:</td>
            <td align="right"><strong>R$ <?=str_price($products_subtotal)?></strong></td>
        </tr>
        <tr>
            <td align="right" colspan="3">Frete:</td>
            <td align="right">
                <form action="" method="post" class="form-inline">
                    <label for="cep">Qual seu CEP?</label>
                    <input type="number" class="form-control" name="cep" id="cep" />
                    <input type="submit" class="btn btn-default" value="Calcular" />
                </form>
                <?php if (isset($shipping["price"])): ?>
                    <strong>R$ <?=str_price($shipping["price"])?> (<?=$shipping["date"]?> dia<?=$shipping["date"] == "1" ? "" : "s"?>)</strong>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="3">Total:</td>
            <?php $frete = (isset($shipping["price"]) ? floatval($shipping["price"]) : 0); ?>
            <td align="right"><strong>R$ <?=str_price(($frete + $products_subtotal))?></strong></td>
        </tr>
    </tbody>
</table>

<hr>
<?php if ($frete >= 0): ?>
    <form action="<?= BASE_URL . "cart/payment_redirect"?>" method="post" class="form-inline" style="float: right;">
        <div class="form-group">
            <label for="payment_type">Forma de pagamento</label>
            <select class="form-control" name="payment_type" id="payment_type">
                <option value="pagseguro_checkout_transparent">
                    Pagseguro Checkout Transparente
                </option>
                <option value="mercadopago_checkout">
                    Mercado Pago
                </option>
                <option value="paypal_checkout">
                    Paypal
                </option>
                <option value="boleto">
                    Boleto
                </option>
            </select>
        </div>
        <input type="submit" class="btn btn-success btn-custom-success" value="Finalizar compra" />
    </form>
<?php endif; ?>