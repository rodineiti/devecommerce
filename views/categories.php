<h1><?=$category_filter->name?></h1>
<div class="row">
    <?php foreach ($products as $key => $product): ?>
        <div class="col-sm-4">
            <?=$this->view("product_item", ["product" => $product])?>
        </div>
    <?php endforeach; ?>
</div>
<div class="row">
    <div class="col">
        <?php for($i = 1; $i <= $pages; $i++): ?>
            <div class="navigation-item <?=($page === $i) ? "navigation-active" : ""?>">
                <a href="<?= BASE_URL ?>categories/show/<?=$category_filter->id?>?page=<?=$i?>"><?=$i?></a>
            </div>
        <?php endfor; ?>
    </div>
</div>
