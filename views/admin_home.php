<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin_menu");?>
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-2 p-2">
                    <div class="card text-center">
                        <div class="card-header">Usuários</div>
                        <div class="card-body">
                            <h5 class="card-title"><?=$count_users?></h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 p-2">
                    <div class="card text-center">
                        <div class="card-header">Grupos</div>
                        <div class="card-body">
                            <h5 class="card-title"><?=$count_groups?></h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 p-2">
                    <div class="card text-center">
                        <div class="card-header">Permissões</div>
                        <div class="card-body">
                            <h5 class="card-title"><?=$count_items?></h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 p-2">
                    <div class="card text-center">
                        <div class="card-header">Marcas</div>
                        <div class="card-body">
                            <h5 class="card-title"><?=$count_brands?></h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 p-2">
                    <div class="card text-center">
                        <div class="card-header">Categorias</div>
                        <div class="card-body">
                            <h5 class="card-title"><?=$count_categories?></h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 p-2">
                    <div class="card text-center">
                        <div class="card-header">Opções</div>
                        <div class="card-body">
                            <h5 class="card-title"><?=$count_options?></h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 p-2">
                    <div class="card text-center">
                        <div class="card-header">Produtos</div>
                        <div class="card-body">
                            <h5 class="card-title"><?=$count_products?></h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 p-2">
                    <div class="card text-center">
                        <div class="card-header">Páginas</div>
                        <div class="card-body">
                            <h5 class="card-title"><?=$count_pages?></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>