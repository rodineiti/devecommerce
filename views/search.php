<h1><?php $this->lang->get("SEARCHING") ?> <strong><?=$searchTerm?></strong></h1>
<div class="row">
    <?php foreach ($products as $key => $product): ?>
        <div class="col-sm-4">
            <?=$this->view("product_item", ["product" => $product])?>
        </div>
    <?php endforeach; ?>
</div>
<div class="row">
    <div class="col">
        <?php for($i = 1; $i <= $pages; $i++): ?>
            <div class="navigation-item <?=($page === $i) ? "navigation-active" : ""?>">
                <a href="<?= BASE_URL ?>?<?php
                    $pageArray = $_GET;
                    $pageArray["page"] = $i;
                    echo http_build_query($pageArray);
                ?>"><?=$i?></a>
            </div>
        <?php endfor; ?>
    </div>
</div>
