<div class="jumbotron">
    <h2>OPSSS...</h2>
    <div class="alert alert-danger">
        Que pena, o pagamento não foi aprovado, houve um erro no processo, nenhuma cobrança foi realizada.
        Tente novamente mais tarde!
    </div>
    <a href="<?=back()?>" class="btn btn-warning">Voltar</a>
</div>