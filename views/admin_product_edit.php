<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin_menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= BASE_URL . "admin/{$redirect}/index"; ?>" class="btn btn-info mb-2">Voltar</a>
            <?php if (isset($_GET["error"]) && $_GET["error"] === "fields"): ?>
                <div class="alert alert-warning">
                    Preencha todos os campos!
                </div>
            <?php endif; ?>
            <?php if (isset($_GET["success"]) && $_GET["success"] === "edit"): ?>
                <div class="alert alert-success">
                    <strong>OK!</strong> Atualizado sucesso.
                </div>
            <?php endif; ?>
            <form method="POST" action="<?= BASE_URL?>admin/<?=$redirect?>/update/<?= $model->id; ?>" enctype="multipart/form-data">
                <div class="row">
                    <div class="col">
                        <h1>Editar produto</h1>
                    </div>
                    <div class="col">
                        <div class="form-group text-right">
                            <input type="submit" value="Editar Produto" class="btn btn-primary" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="category_id">Categoria:</label>
                            <select name="category_id" id="category_id" class="form-control" required>
                                <option value="">Selecione</option>
                                <?php foreach ($categories as $category): ?>
                                    <option value="<?=$category["id"]?>"
                                        <?=selected($model->category_id == $category["id"])?>><?=$category["name"]?></option>
                                    <?php if (count($category["subs"])): ?>
                                        <?=$this->view("subcategory", [
                                            "type" => "options",
                                            "subs" => $category["subs"],
                                            "level" => 1,
                                            "category" => isset($model->category_id) ? $model->category_id : null
                                        ]);?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="brand_id">Marca:</label>
                            <select name="brand_id" id="brand_id" class="form-control" required>
                                <option value="">Selecione</option>
                                <?php foreach ($brands as $brand): ?>
                                    <option value="<?=$brand->id?>"
                                        <?=selected($model->brand_id == $brand->id)?>><?=$brand->name?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name">Nome:</label>
                    <input type="text" name="name" id="name" class="form-control" value="<?=$model->name?>" required />
                </div>
                <div class="form-group">
                    <label for="description">Descrição:</label>
                    <textarea name="description" id="description" class="form-control"><?=$model->description?></textarea>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="stock">Estoque:</label>
                            <input type="number" name="stock" id="stock" class="form-control" value="<?=$model->stock?>" required />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="price_from">Preço (de):</label>
                            <input type="text" name="price_from" id="price_from" class="form-control" value="<?=$model->price_from?>" required />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="price">Preço (por):</label>
                            <input type="text" name="price" id="price" class="form-control" value="<?=$model->price?>" required />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="weight">Peso (em Kg):</label>
                            <input type="number" name="weight" id="weight" class="form-control" value="<?=$model->weight?>" required />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="width">Largura (em Cm):</label>
                            <input type="number" name="width" id="width" class="form-control" value="<?=$model->width?>" required />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="height">Altura (em Cm):</label>
                            <input type="number" name="height" id="height" class="form-control" value="<?=$model->height?>" required />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="length">Comprimeito (em Cm):</label>
                            <input type="number" name="length" id="length" class="form-control" value="<?=$model->length?>" required />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="diameter">Diametro (em cm):</label>
                            <input type="number" name="diameter" id="diameter" class="form-control" value="<?=$model->diameter?>" required />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="checkbox" name="featured" id="featured" value="1" <?=checked($model->featured == "1")?> />
                            <label for="featured">Em destaque:</label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="checkbox" name="sale" id="sale" value="1" <?=checked($model->sale == "1")?> />
                            <label for="sale">Em promoção:</label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="checkbox" name="bestseller" id="bestseller" value="1" <?=checked($model->bestseller == "1")?> />
                            <label for="bestseller">Mais vendidos:</label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="checkbox" name="new_add" id="new_add" value="1" <?=checked($model->new_add == "1")?> />
                            <label for="new_add">Novo produto</label>
                        </div>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <?php foreach ($options as $option): ?>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="option-<?=$option->id?>"><?=$option->name?></label>
                                <input type="text" name="options[<?=$option->id?>]"
                                       id="option-<?=$option->id?>"
                                       value="<?= isset($optionsProduct["options"][$option->id]) ? $optionsProduct["options"][$option->id] : "" ?>"
                                       class="form-control" />
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>

                <hr>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="row">
                            <?php foreach ($imagesProduct as $image): ?>
                                <div class="col-sm-3">
                                    <div class="card">
                                        <div class="card-body">
                                            <img src="<?=$image->url?>" alt="image" class="img-fluid" />
                                        </div>
                                        <div class="card-footer text-center">
                                            <a href="<?=BASE_URL."admin/{$redirect}/delete_image/".$image->product_id."/".$image->id;?>"
                                               class="btn btn-danger">Deletar</a>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>

                        <div class="row">
                            <div class="col">
                                <label for="images-area">Imagens do produto</label><br>
                                <button class="btn btn-primary mt-1 mb-2" id="btn-add-input">+</button>
                                <div class="form-group area-images mt-1">
                                    <input type="file" name="images[]" class="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <strong>Avaliação</strong>
                        <?php if ($model->rating != "0"): ?>
                            <?php for ($i = 0; $i < intval($model->rating); $i++): ?>
                                <img src="<?=image("star.png")?>" alt="star" height="20" />
                            <?php endfor; ?>
                        <?php endif; ?>
                        <hr>
                        <?php foreach ($model->productRates as $rate): ?>
                            <?php if ($rate->points != "0"): ?>
                                <?php for ($i = 0; $i < intval($rate->points); $i++): ?>
                                    <img src="<?=image("star.png")?>" alt="star" height="20" />
                                <?php endfor; ?>
                            <?php endif; ?>
                            <p><strong><?=$rate->user->name?></strong></p>
                            <p>"<?=$rate->description?>"</p>
                            <small><?=date("d/m/Y H:i", strtotime($rate->created_at))?></small>
                            <a href="<?=BASE_URL."admin/{$redirect}/delete_rate/".$rate->product_id."/".$rate->id;?>"
                               class="btn btn-danger">Deletar</a>
                            <hr>
                        <?php endforeach; ?>
                    </div>
                </div>
            </form>
            <hr>
            <h1>Adicionar avaliação</h1>
            <form action="<?= BASE_URL?>admin/<?=$redirect?>/add_rate/<?= $model->id; ?>" method="post">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="user_id">Usuário:</label>
                            <select name="user_id" id="user_id" class="form-control" required>
                                <option value="">Selecione</option>
                                <?php foreach ($users as $user): ?>
                                    <option value="<?=$user->id?>"><?=$user->name?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="points">Avaliação:</label>
                            <select name="points" id="points" class="form-control" required>
                                <option value="">Selecione</option>
                                <?php for ($i = 1; $i <= 5; $i++): ?>
                                    <option value="<?=$i?>"><?=$i?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="description">Avaliação:</label>
                            <textarea name="description" id="description" class="form-control" required></textarea>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <input type="submit" value="Adicionar avaliação" class="btn btn-primary" />
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>