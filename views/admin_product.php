<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin_menu");?>
        </div>
        <div class="col-md-9">
            <h1 class="text-center">Lista de Produtos</h1>
            <hr>
            <?php if (hasPermission("{$prefix}-create")): ?>
                <a href="<?= BASE_URL . "admin/{$redirect}/create"; ?>" class="btn btn-primary mb-2">Adicionar</a>
            <?php endif; ?>
            <?php if (isset($_GET["error"])): ?>
                <div class="alert alert-danger">
                    Opss. Ocorreu um erro no processamento, tente mais tarde.
                </div>
            <?php endif; ?>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Categoria</th>
                    <th scope="col">Nome do Produto</th>
                    <th scope="col">Estoque</th>
                    <th scope="col">Preço</th>
                    <th scope="col">Criado em</th>
                    <th scope="col">Opções</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($list as $item): ?>
                    <tr>
                        <th scope="row"><?= $item->id ?></th>
                        <td><?= $item->category ?></td>
                        <td>
                            <?= $item->name ?>
                            <br>
                            <small><?= $item->brand ?></small>
                        </td>
                        <td><?= $item->stock ?></td>
                        <td>
                            <small style="text-decoration: line-through;">R$ <?= str_price($item->price_from) ?></small>
                            <br>
                            R$ <?= str_price($item->price) ?>
                        </td>
                        <td><?= $item->created_at ?></td>
                        <td>
                            <?php if (hasPermission("{$prefix}-edit")): ?>
                                <a href="<?= BASE_URL . "admin/{$redirect}/edit/" . $item->id; ?>" class="btn btn-info">Editar</a>
                            <?php endif; ?>
                            <?php if (hasPermission("{$prefix}-destroy")): ?>
                                <a href="<?= BASE_URL . "admin/{$redirect}/destroy/" . $item->id; ?>" class="btn btn-danger"
                                   onclick="return confirm('Confirmar a exclusão?')"
                                >Deletar</a>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <nav aria-label="...">
                <ul class="pagination">
                    <?php for($i = 1; $i <= $pages; $i++): ?>
                    <li class="page-item <?=($page === $i) ? "active" : ""?>">
                        <a class="page-link" href="<?= BASE_URL . "admin/{$redirect}/index" ?>?<?php
                        $pageArray["page"] = $i;
                        echo http_build_query($pageArray);
                        ?>"><?=$i?></a>
                    </li>
                    <?php endfor; ?>
                </ul>
            </nav>
        </div>
    </div>
</div>