<div class="card">
    <div class="card-body">
        <div class="nav flex-column nav-pills" aria-orientation="vertical">
            <a class="nav-link border mb-1 <?=setMenuActive(["admin/home"])?>" href="<?= BASE_URL . "admin/home";?>">
                Home
            </a>
            <?php if (hasPermission("users-index")): ?>
            <a class="nav-link border mb-1 <?=setMenuActive(["admin/users/index","admin/users/create","admin/users/edit"])?>" href="<?= BASE_URL . "admin/users/index";?>">
                Usuários
            </a>
            <?php endif; ?>
            <?php if (hasPermission("permission-groups-index")): ?>
                <a class="nav-link border mb-1 <?=setMenuActive(["admin/permissiongroups/index","admin/permissiongroups/create","admin/permissiongroups/edit"])?>" href="<?= BASE_URL . "admin/permissiongroups/index";?>">
                    Grupo de Permissões
                </a>
            <?php endif; ?>
            <?php if (hasPermission("permission-items-index")): ?>
                <a class="nav-link border mb-1 <?=setMenuActive(["admin/permissionitems/index","admin/permissionitems/create","admin/permissionitems/edit"])?>" href="<?= BASE_URL . "admin/permissionitems/index";?>">
                    Itens de Permissões
                </a>
            <?php endif; ?>
            <?php if (hasPermission("brands-index")): ?>
                <a class="nav-link border mb-1 <?=setMenuActive(["admin/brands/index","admin/brands/create","admin/brands/edit"])?>" href="<?= BASE_URL . "admin/brands/index";?>">
                    Marcas
                </a>
            <?php endif; ?>
            <?php if (hasPermission("categories-index")): ?>
                <a class="nav-link border mb-1 <?=setMenuActive(["admin/categories/index","admin/categories/create","admin/categories/edit"])?>" href="<?= BASE_URL . "admin/categories/index";?>">
                    Categorias
                </a>
            <?php endif; ?>
            <?php if (hasPermission("products-index")): ?>
                <a class="nav-link border mb-1 <?=setMenuActive(["admin/options/index","admin/options/create","admin/options/edit"])?>" href="<?= BASE_URL . "admin/options/index";?>">
                    Opções de produtos
                </a>
            <?php endif; ?>
            <?php if (hasPermission("products-index")): ?>
                <a class="nav-link border mb-1 <?=setMenuActive(["admin/products/index","admin/products/create","admin/products/edit"])?>" href="<?= BASE_URL . "admin/products/index";?>">
                    Produtos
                </a>
            <?php endif; ?>
            <?php if (hasPermission("pages-index")): ?>
                <a class="nav-link border mb-1 <?=setMenuActive(["admin/pages/index","admin/pages/create","admin/pages/edit"])?>" href="<?= BASE_URL . "admin/pages/index";?>">
                    Páginas
                </a>
            <?php endif; ?>
        </div>
    </div>
</div>