<?php
ob_start(); // controlar o cache da aplicação para ter apenas 1 output

session_start();

$timezone = date_default_timezone_set("America/Sao_Paulo");

require "vendor/autoload.php";
require "config.php";
require "helpers.php";

try {
    \PagSeguro\Library::initialize();
    \PagSeguro\Library::cmsVersion()->setName(CONF_SITENAME_CHECKOUT)->setRelease(CONF_VERSION_CHECKOUT);
    \PagSeguro\Library::moduleVersion()->setName(CONF_SITENAME_CHECKOUT)->setRelease(CONF_VERSION_CHECKOUT);

    \PagSeguro\Configuration\Configure::setEnvironment(CONFIG_PAGSEGURO_ENVIRONMENT);
    \PagSeguro\Configuration\Configure::setAccountCredentials(CONF_EMAIL_PAGSEGURO, CONF_TOKEN_PAGSEGURO);
    \PagSeguro\Configuration\Configure::setCharset(CONF_CHARSET_PAGSEGURO);
    \PagSeguro\Configuration\Configure::setLog(true, CONF_FILE_LOG_PAGSEGURO);
} catch (Exception $exception) {
    dd($exception->getMessage());
}

(new Src\Core\Core())->run();

ob_end_flush(); // envia o output e limpa o cache